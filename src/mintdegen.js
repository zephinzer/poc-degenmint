_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([[36], {
    "4oX2": function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return r
        }
        ));
        var a = s("nKUr")
          , n = s("/RHU")
          , l = s("Ksly")
          , c = s("Y/GY");
        function r() {
            return Object(a.jsx)("footer", {
                className: "text-white pb-4 bg-black",
                children: Object(a.jsxs)("div", {
                    className: "grid xs:grid-cols-1 sm:grid-cols-3 justify-between px-6",
                    children: [Object(a.jsx)("div", {}), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmTYLiQqjTKfEFZ6MVNaahsjAR51hcSAhaxFrP4kKvQ3JW",
                            className: "h-36 sm:h-48"
                        })
                    }), Object(a.jsxs)("div", {
                        className: "self-end xs:pt-8 pt-2",
                        children: [Object(a.jsxs)("div", {
                            className: "flex space-x-4 sm:justify-end xs:justify-center",
                            children: [Object(a.jsx)("div", {
                                className: "cursor-pointer",
                                children: Object(a.jsx)("a", {
                                    target: "_blank",
                                    href: "https://discord.gg/madmeerkat",
                                    children: Object(a.jsx)(n.a, {
                                        height: "32",
                                        width: "32",
                                        className: "text-white"
                                    })
                                })
                            }), Object(a.jsx)("div", {
                                className: "cursor-pointer",
                                children: Object(a.jsx)("a", {
                                    target: "_blank",
                                    href: "https://twitter.com/MadMeerkatNFT",
                                    children: Object(a.jsx)(l.a, {
                                        height: "32",
                                        width: "32",
                                        className: "text-white"
                                    })
                                })
                            }), Object(a.jsx)("div", {
                                className: "cursor-pointer",
                                children: Object(a.jsx)("a", {
                                    target: "_blank",
                                    href: "https://mmfinance.gitbook.io/mad-bucks/about-usdmad/what-is-usdmad",
                                    children: Object(a.jsx)(c.a, {
                                        height: "32",
                                        width: "32",
                                        className: "text-white"
                                    })
                                })
                            })]
                        }), Object(a.jsx)("div", {
                            className: "flex sm:justify-end xs:justify-center",
                            children: Object(a.jsx)("p", {
                                className: "text-center text-gray-600 text-base pt-2 pb-1",
                                children: "Mad Meerkat 2022"
                            })
                        }), Object(a.jsxs)("div", {
                            className: "flex sm:justify-end xs:justify-center text-xs cursor-pointer",
                            onClick: function() {
                                return window.open("https://nebkas.ro/", "_blank")
                            },
                            children: [Object(a.jsx)("span", {
                                className: "mr-2 flex justify-items-center items-center text-gray-500",
                                children: "RPC Powered by"
                            }), Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/nebkas.png",
                                className: "h-7 sm:h-7"
                            })]
                        })]
                    })]
                })
            })
        }
    },
    "AU+d": function(e, t, s) {
        "use strict";
        var a = s("q1tI")
          , n = s.n(a);
        function l() {
            return (l = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var s = arguments[t];
                    for (var a in s)
                        Object.prototype.hasOwnProperty.call(s, a) && (e[a] = s[a])
                }
                return e
            }
            ).apply(this, arguments)
        }
        t.a = function(e, t) {
            void 0 === t && (t = {});
            var c, r = t, i = r.volume, o = void 0 === i ? 1 : i, d = r.playbackRate, u = void 0 === d ? 1 : d, x = r.soundEnabled, m = void 0 === x || x, p = r.interrupt, b = void 0 !== p && p, j = r.onload, h = function(e, t) {
                if (null == e)
                    return {};
                var s, a, n = {}, l = Object.keys(e);
                for (a = 0; a < l.length; a++)
                    s = l[a],
                    t.indexOf(s) >= 0 || (n[s] = e[s]);
                return n
            }(r, ["id", "volume", "playbackRate", "soundEnabled", "interrupt", "onload"]), g = n.a.useRef(null), f = n.a.useRef(!1), v = n.a.useState(null), O = v[0], y = v[1], w = n.a.useState(null), k = w[0], N = w[1], M = function() {
                "function" === typeof j && j.call(this),
                f.current && y(1e3 * this.duration()),
                N(this)
            };
            c = function() {
                return s.e(44).then(s.t.bind(null, "HlzF", 7)).then((function(t) {
                    f.current || (g.current = t.Howl,
                    f.current = !0,
                    new g.current(l({
                        src: Array.isArray(e) ? e : [e],
                        volume: o,
                        rate: u,
                        onload: M
                    }, h)))
                }
                )),
                function() {
                    f.current = !1
                }
            }
            ,
            Object(a.useEffect)(c, []),
            n.a.useEffect((function() {
                g.current && k && N(new g.current(l({
                    src: Array.isArray(e) ? e : [e],
                    volume: o,
                    onload: M
                }, h)))
            }
            ), [JSON.stringify(e)]),
            n.a.useEffect((function() {
                k && (k.volume(o),
                k.rate(u))
            }
            ), [o, u]);
            var C = n.a.useCallback((function(e) {
                "undefined" === typeof e && (e = {}),
                k && (m || e.forceSoundEnabled) && (b && k.stop(),
                e.playbackRate && k.rate(e.playbackRate),
                k.play(e.id))
            }
            ), [k, m, b])
              , S = n.a.useCallback((function(e) {
                k && k.stop(e)
            }
            ), [k])
              , A = n.a.useCallback((function(e) {
                k && k.pause(e)
            }
            ), [k]);
            return [C, {
                sound: k,
                stop: S,
                pause: A,
                duration: O
            }]
        }
    },
    JXCf: function(e) {
        e.exports = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"}]')
    },
    MaBg: function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return r
        }
        ));
        var a = s("Z6YE")
          , n = s("q1tI")
          , l = s("meUc")
          , c = s.n(l);
        function r(e, t) {
            var s = arguments.length > 2 && void 0 !== arguments[2] && arguments[2]
              , l = Object(a.b)()
              , r = l.library
              , i = l.account;
            return Object(n.useMemo)((function() {
                if (null !== r && void 0 !== r && r.provider)
                    return new (new c.a(null === r || void 0 === r ? void 0 : r.provider).eth.Contract)(t,e)
            }
            ), [e, t, s, r, i])
        }
    },
    MwE2: function(e, t, s) {
        (window.__NEXT_P = window.__NEXT_P || []).push(["/mint", function() {
            return s("ounl")
        }
        ])
    },
    QydO: function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return c
        }
        ));
        var a = s("nKUr")
          , n = s("q1tI")
          , l = s("akeB");
        function c() {
            var e = Object(n.useState)(!0)
              , t = (e[0],
            e[1]);
            Object(n.useEffect)((function() {
                setTimeout((function() {
                    return t(!1)
                }
                ), 1500)
            }
            ), []);
            var s = Object(l.c)({
                scale: [1, 1.2, "easeInQuad"]
            })
              , c = Object(l.c)({
                scale: [1, .8, "easeInQuad"]
            })
              , r = Object(l.c)({
                scale: [1.2, .5, "easeInQuad"]
            })
              , i = Object(l.c)({
                scale: [1.2, 1, "easeInQuad"]
            });
            return Object(a.jsxs)("div", {
                children: [Object(a.jsxs)("div", {
                    className: "grid xs:grid-cols-1 lg:grid-cols-2 mt-24 justify-center xs:px-8 lg:px-64",
                    children: [Object(a.jsx)("div", {
                        className: "mmt",
                        ref: c.ref,
                        children: Object(a.jsx)("div", {
                            className: "flex justify-center",
                            children: Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/4.png",
                                width: 350,
                                className: "mt-4 rounded-xl"
                            })
                        })
                    }), Object(a.jsx)("div", {
                        className: "text-white xs:text-md sm:text-lg grid grid-cols-1 leading-relaxed xs:text-center lg:text-center xs:px-8 lg:px-0 xs:pt-12 lg:pt-0 items-center",
                        children: Object(a.jsxs)("div", {
                            children: [Object(a.jsx)("div", {
                                className: "text-3xl pb-4 text-center",
                                children: "The Degen Mad Meerkat Collection is the symbol of True Madness within MM."
                            }), Object(a.jsx)("div", {
                                children: "Loving the thrill of celebrating highs and avenging lows, this is what it means to be on an Adventure like no other. MM has and will always be a movement for those who burn bright in the Madness no matter what happens."
                            }), Object(a.jsx)("br", {}), Object(a.jsxs)("div", {
                                className: "text-lg pb-4 text-center",
                                children: ["Supply: 10,010", Object(a.jsx)("br", {}), "10,000 to be minted and airdropped", Object(a.jsx)("br", {}), "10 Legendaries where 2 will be owned by team, 8 will be auctioned."]
                            }), Object(a.jsx)("div", {
                                className: "text-2xl pb-4 text-center",
                                children: "#MAD4LIFE"
                            })]
                        })
                    })]
                }), Object(a.jsxs)("div", {
                    className: "grid xs:grid-cols-1 lg:grid-cols-2 justify-center xs:px-8 lg:px-64",
                    children: [Object(a.jsx)("div", {
                        className: "mmt",
                        ref: r.ref,
                        children: Object(a.jsx)("div", {
                            className: "flex justify-center",
                            children: Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/13.png",
                                width: 350,
                                className: "mt-4"
                            })
                        })
                    }), Object(a.jsx)("div", {
                        className: "text-white xs:text-md sm:text-lg grid grid-cols-1 leading-relaxed xs:text-center lg:text-center xs:px-8 lg:px-0 xs:pt-12 lg:pt-0 items-center",
                        children: Object(a.jsxs)("div", {
                            children: [Object(a.jsx)("div", {
                                className: "text-3xl pb-4 text-center",
                                children: "The Degen Mad Meerkat Collection represents our gratitude to the OG across all chains."
                            }), Object(a.jsxs)("div", {
                                children: [Object(a.jsx)("span", {
                                    className: "text-yellow-500 font-bold text-2xl",
                                    children: "All $MAD raised from the Degen Mad Meerkat mint will be burnt."
                                }), Object(a.jsx)("br", {}), "Assuming 10,000 Degen MM is sold out by the whitelist round, approximately 357,500 $MAD (USD 5,000,000 as of today\u2019s price) will be burnt forever."]
                            })]
                        })
                    })]
                }), Object(a.jsxs)("div", {
                    className: "grid xs:grid-cols-1 lg:grid-cols-2 mb-24 justify-center xs:px-8 lg:px-64",
                    children: [Object(a.jsx)("div", {
                        className: "mmt",
                        ref: i.ref,
                        children: Object(a.jsx)("div", {
                            className: "flex justify-center",
                            children: Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/12.png",
                                width: 350,
                                className: "mt-4"
                            })
                        })
                    }), Object(a.jsx)("div", {
                        className: "text-white xs:text-md sm:text-lg grid grid-cols-1 leading-relaxed xs:text-center lg:text-center xs:px-8 lg:px-0 xs:pt-12 lg:pt-0 items-center",
                        children: Object(a.jsxs)("div", {
                            children: [Object(a.jsxs)("div", {
                                className: "text-xl pb-4 text-center",
                                children: ["Degen Mad Meerkat will be airdropped to those that hold at least 1 Genesis Mad Meerkat, 4 Cro Mad Meerkats or 5 Treehouses.", Object(a.jsx)("br", {}), " Visit", " ", Object(a.jsx)("a", {
                                    className: "text-pink-500 hover:text-pink-200",
                                    href: "/airdrop",
                                    children: "Airdrop Page"
                                }), " ", "for more details."]
                            }), Object(a.jsxs)("div", {
                                className: "text-xl pb-4 text-center",
                                children: ["Degen Mad Meerkat can also be minted using $MAD tokens. ", Object(a.jsx)("br", {}), " ", "Visit", " ", Object(a.jsx)("a", {
                                    className: "text-pink-500 hover:text-pink-200",
                                    href: "/mint",
                                    children: "Mint Page"
                                }), " ", "for more info."]
                            })]
                        })
                    })]
                }), Object(a.jsx)("div", {
                    className: "flex my-20 justify-center xs:px-8 lg:px-8 items-center",
                    children: Object(a.jsx)("div", {
                        className: "text-white xs:text-md sm:text-lg grid grid-cols-1 leading-relaxed xs:text-center lg:text-center xs:px-8 lg:px-0 xs:pt-0 lg:pt-0 items-center",
                        children: Object(a.jsxs)("div", {
                            children: [Object(a.jsx)("div", {
                                className: "text-2xl pb-4 pt-8",
                                children: "Read the full details of Degen Mad Meerkat on Medium"
                            }), Object(a.jsx)("div", {
                                className: "pb-2",
                                children: Object(a.jsx)("a", {
                                    href: "https://medium.com/@madmeerkat0/dmm-mint-details-9f19a85283f4",
                                    target: "_blank",
                                    className: "text-pink-500 hover:text-pink-300",
                                    children: "DMM Mint Details"
                                })
                            }), Object(a.jsx)("div", {
                                className: "pb-2",
                                children: Object(a.jsx)("a", {
                                    href: "https://medium.com/@madmeerkat0/the-next-frontier-of-madness-728667ef8a86",
                                    target: "_blank",
                                    className: "text-pink-500 hover:text-pink-300",
                                    children: "The Next Frontier Of Madness & DMM Utilities."
                                })
                            })]
                        })
                    })
                }), Object(a.jsx)("div", {
                    className: "text-gray-200 flex justify-center text-4xl font-bold text-center sm:pt-12",
                    children: "MM World."
                }), Object(a.jsxs)("div", {
                    className: "text-gray-600 xs:text-base text-lg pt-1 text-center mb-16",
                    children: ["Join us in our MAD Adventure. ", Object(a.jsx)("br", {}), "#MAD4LIFE"]
                }), Object(a.jsxs)("div", {
                    className: "grid xs:grid-cols-1 lg:grid-cols-3 my-32 justify-center xs:mx-32 md:mx-64 lg:mx-40 lg:px-32 xl:mx-64 xl:px-80 items-center space-y-4",
                    ref: s.ref,
                    children: [Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmTVMcAA4qdtm2YXbub5QAzDYPgXhjkP6x3LQp7CAD1ARr/mmf.png",
                            width: 150,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://mm.finance/", "_blank")
                            }
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmTVMcAA4qdtm2YXbub5QAzDYPgXhjkP6x3LQp7CAD1ARr/mmo.png",
                            width: 160,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://vaults.mm.finance/vault", "_blank")
                            }
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmTVMcAA4qdtm2YXbub5QAzDYPgXhjkP6x3LQp7CAD1ARr/svn.png",
                            width: 150,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://svn.finance/", "_blank")
                            }
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/metf.svg",
                            width: 150,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://metf.finance", "_blank")
                            }
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/mma.svg",
                            width: 150,
                            className: "cursor-default transform hover:scale-110"
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/mmt.svg",
                            width: 150,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://mmtreehouse.io", "_blank")
                            }
                        })
                    }), Object(a.jsx)("div", {
                        className: "flex justify-center sm:col-span-3",
                        children: Object(a.jsx)("img", {
                            src: "https://madmeerkat.mypinata.cloud/ipfs/QmdWq4f17TkXx9hyx5Epy8LQMzqbhC6y4tLS7urBJCZb1g/burrow.svg",
                            width: 150,
                            className: "cursor-pointer transform hover:scale-110",
                            onClick: function() {
                                window.open("https://mmf.money", "_blank")
                            }
                        })
                    })]
                })]
            })
        }
    },
    UZjq: function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return a
        }
        ));
        var a = function(e) {
            return fetch(e).then((function(e) {
                return e.json()
            }
            ))
        }
    },
    ounl: function(e, t, s) {
        "use strict";
        s.r(t),
        s.d(t, "default", (function() {
            return q
        }
        ));
        var a = s("nKUr")
          , n = s("q1tI")
          , l = s("5/yq")
          , c = s("4oX2")
          , r = s("OPvz")
          , i = s("QydO")
          , o = s("vJKn")
          , d = s.n(o)
          , u = s("xvhg")
          , x = s("rg98")
          , m = s("zZBi")
          , p = s("JXCf")
          , b = s("MaBg")
          , j = s("Z3qu")
          , h = s("FGyW")
          , g = s("eI1P")
          , f = s("Wgwc")
          , v = s.n(f)
          , O = s("QgiU")
          , y = s.n(O)
          , w = s("NTZE")
          , k = s("Z6YE")
          , N = s("meUc")
          , M = s.n(N)
          , C = s("AU+d")
          , S = s("xQut")
          , A = s("LmB1")
          , B = s("xXRr")
          , z = s("UZjq");
        v.a.extend(y.a);
        var P = function() {
            var e, t, s, l, c, r, i, o, j, f, v, O, y, N = Object(k.b)().account, A = Object(n.useState)(0), P = A[0], Q = A[1], F = Object(n.useState)(!1), W = F[0], D = F[1], T = Object(b.a)(g.f, p);
            function q() {
                return (q = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!N) {
                                    e.next = 14;
                                    break
                                }
                                return D(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === T || void 0 === T || null === (t = T.methods) || void 0 === t ? void 0 : t.approve(g.d, g.g).send({
                                    from: N,
                                    value: "0"
                                });
                            case 5:
                                Object(h.b)("You have successfully approved $MAD usage!", {
                                    autoClose: 3e4
                                }),
                                e.next = 11;
                                break;
                            case 8:
                                e.prev = 8,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 11:
                                D(!1),
                                e.next = 15;
                                break;
                            case 14:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 15:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 8]])
                }
                )))).apply(this, arguments)
            }
            function R() {
                return (R = Object(x.a)(d.a.mark((function e(t) {
                    var s, a;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                return e.prev = 0,
                                e.next = 3,
                                Object(z.a)("/api/my_eth_token?address=".concat(t));
                            case 3:
                                a = e.sent,
                                Q(parseInt(null !== (s = null === a || void 0 === a ? void 0 : a.data) && void 0 !== s ? s : 0)),
                                e.next = 10;
                                break;
                            case 7:
                                e.prev = 7,
                                e.t0 = e.catch(0),
                                console.log("Failed");
                            case 10:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[0, 7]])
                }
                )))).apply(this, arguments)
            }
            Object(n.useEffect)((function() {
                N && function(e) {
                    R.apply(this, arguments)
                }(N)
            }
            ), [N]);
            var E = Object(n.useState)(.75)
              , J = E[0]
              , Y = E[1]
              , _ = Object(n.useState)(!1)
              , I = _[0]
              , U = _[1]
              , Z = Object(C.a)("/sounds/glug.wav", {
                playbackRate: J,
                volume: .3
            })
              , L = Object(u.a)(Z, 1)[0]
              , K = Object(n.useState)(1)
              , G = K[0]
              , H = K[1]
              , $ = Object(w.b)([[g.d, "presale1Active"], [g.d, "presale1reservedDegens"], [g.d, "degenPricePresale1"], [g.d, "isWhitelist1", N], [g.d, "MAX_DEGEN_PURCHASE"], [g.d, "balanceOf", N], [g.d, "MAX_DEGEN_PER_ROUND"], [g.f, "allowance", N, g.d], [g.f, "balanceOf", N], [g.d, "amountPurchased1", N]]).data
              , X = null !== (e = null === $ || void 0 === $ ? void 0 : $[0]) && void 0 !== e && e
              , V = null !== (t = null === $ || void 0 === $ ? void 0 : $[1].toString()) && void 0 !== t ? t : 0
              , ee = null !== (s = null === $ || void 0 === $ ? void 0 : $[2].toString()) && void 0 !== s ? s : "55000000000000000000"
              , te = null !== (l = null === $ || void 0 === $ ? void 0 : $[3]) && void 0 !== l && l
              , se = parseFloat(null !== (c = null === $ || void 0 === $ ? void 0 : $[4].toString()) && void 0 !== c ? c : 0)
              , ae = parseInt(null !== (r = null === $ || void 0 === $ ? void 0 : $[5].toString()) && void 0 !== r ? r : 0)
              , ne = parseFloat(null !== (i = null === $ || void 0 === $ ? void 0 : $[6].toString()) && void 0 !== i ? i : 0)
              , le = null === $ || void 0 === $ || null === (o = $[7]) || void 0 === o ? void 0 : o.toString()
              , ce = null !== (j = null === $ || void 0 === $ ? void 0 : $[8]) && void 0 !== j ? j : 0
              , re = parseFloat(null !== (f = null === $ || void 0 === $ ? void 0 : $[9].toString()) && void 0 !== f ? f : 0)
              , ie = !te || !X || "0" === V || "0" === le || 0 === P || re === ne
              , oe = Object(b.a)(g.d, m)
              , de = "Genesis Round"
              , ue = (parseInt(ee) * G).toLocaleString("fullwide", {
                useGrouping: !1
            });
            function xe() {
                return (xe = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!N) {
                                    e.next = 17;
                                    break
                                }
                                return U(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === oe || void 0 === oe ? void 0 : oe.methods.presalemintDegen1(G, ue).estimateGas({
                                    from: N,
                                    value: "0"
                                });
                            case 5:
                                return t = e.sent,
                                e.next = 8,
                                null === oe || void 0 === oe ? void 0 : oe.methods.presalemintDegen1(G, ue).send({
                                    from: N,
                                    value: "0",
                                    gasLimit: Math.trunc(1.4 * t),
                                    gasPrice: 8e12
                                });
                            case 8:
                                Object(h.b)("You have successfully minted ".concat(G, " DMM!"), {
                                    autoClose: 3e4
                                }),
                                e.next = 14;
                                break;
                            case 11:
                                e.prev = 11,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 14:
                                U(!1),
                                e.next = 18;
                                break;
                            case 17:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 18:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 11]])
                }
                )))).apply(this, arguments)
            }
            return Object(a.jsxs)(a.Fragment, {
                children: [Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "text-sm text-gray-200 flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 text-center lg:col-span-1 relative bg-gradient-to-b from-emerald-900 via-[#00150d] to-teal-900 rounded-lg p-2 cursor-default",
                        children: ["Connected to ", null === N || void 0 === N ? void 0 : N.slice(0, 4), "...", null === N || void 0 === N ? void 0 : N.slice(-4)]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-emerald-900 via-[#00150d] to-teal-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 1: Secure Whitelist Snapshot"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4",
                            children: ["Ensure a Genesis Mad Meerkat is held in this wallet from ", Object(a.jsx)("br", {}), "Saturday 14th May 2022 9AM UTC onwards"]
                        }), Object(a.jsxs)("p", {
                            className: "pt-8 text-2xl text-gray-300 px-4",
                            children: ["You have ", P, " Genesis Mad Meerkat", P > 0 ? "s" : ""]
                        }), Object(a.jsxs)("p", {
                            className: "text-base text-gray-400 px-4 pb-4",
                            children: ["Purchase a Genesis Mad Meerkat from", " ", Object(a.jsx)("a", {
                                href: "https://opensea.io/collection/mad-meerkat-burrow",
                                target: "_blank",
                                className: "text-emerald-500 hover:text-emerald-200 cursor-pointer",
                                children: "Opensea"
                            }), " "]
                        }), te ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsx)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: "You have secured a whitelist spot!"
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["Checkmark will turn green ", Object(a.jsx)("br", {}), "once your wallet is whitelisted"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-emerald-900 via-[#00150d] to-teal-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 mx-8",
                            children: "Step 2: Load up $MAD Token"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Maximum of 5 DMM can be minted if you are whitelisted"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Each DMM will cost 55 $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "pt-8 text-2xl text-gray-300 px-4",
                            children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (v = null === ce || void 0 === ce ? void 0 : ce.toString()) && void 0 !== v ? v : "0", "ether")).toLocaleString(void 0, {
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 4
                            }), " ", Object(a.jsx)("div", {
                                className: "text-xs text-gray-400",
                                children: "Keep some CRO for transaction fees"
                            })]
                        }), Object(a.jsxs)("p", {
                            className: "text-base text-gray-400 px-4 pb-4",
                            children: ["Get more $MAD by staking Treehouses on", " ", Object(a.jsx)("a", {
                                href: "https://mmtreehouse.io",
                                target: "_blank",
                                className: "text-emerald-500 hover:text-emerald-200 cursor-pointer",
                                children: "MM Treehouse"
                            }), " ", Object(a.jsx)("br", {}), " or purchase from", " ", Object(a.jsx)("a", {
                                href: "https://mm.finance/swap?outputCurrency=0x212331e1435A8df230715dB4C02B2a3A0abF8c61",
                                target: "_blank",
                                className: "text-emerald-500 hover:text-emerald-200 cursor-pointer",
                                children: "MM.Finance"
                            })]
                        }), parseFloat(M.a.utils.fromWei(null !== (O = null === ce || void 0 === ce ? void 0 : ce.toString()) && void 0 !== O ? O : "0", "ether")) >= 55 ? Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " "]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsx)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: "Get More MAD!"
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-emerald-900 via-[#00150d] to-teal-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 3: Approve usage of $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4 pb-4",
                            children: ["One Time Approval required ", Object(a.jsx)("br", {}), " to allow deduction of $MAD during minting"]
                        }), "0" !== le ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsx)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: "Wallet Approved!"
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("button", {
                                disabled: W,
                                className: "text-white rounded-xl p-2 bg-gradient-to-r from-emerald-600 to-teal-600 hover:from-emerald-700 hover:to-teal-700",
                                onClick: function() {
                                    !function() {
                                        q.apply(this, arguments)
                                    }()
                                },
                                children: W ? Object(a.jsx)("div", {
                                    className: "pl-8 pr-4",
                                    children: Object(a.jsx)(B.a, {})
                                }) : "Approve Usage"
                            }), Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40 pt-4"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["Checkmark will turn green ", Object(a.jsx)("br", {}), "once your wallet has approved"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-emerald-900 via-[#00150d] to-teal-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 4: Hold!"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4 pb-4",
                            children: ["Genesis Mad Meerkat must be", Object(a.jsx)("br", {}), "in the wallet at the point of minting"]
                        }), P > 0 ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsxs)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: ["You have ", P, " Genesis Mad Meerkat"]
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["You have ", P, " Genesis Mad Meerkat"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsxs)("div", {
                    className: "grid grid-cols-1 m-4",
                    children: [Object(a.jsx)("div", {
                        className: "lg:block xs:hidden flex place-self-center"
                    }), Object(a.jsx)("div", {
                        className: "grid gap-8 items-start justify-center p-4",
                        children: Object(a.jsxs)("div", {
                            className: "relative",
                            children: [Object(a.jsx)("div", {
                                className: "absolute -inset-2 bg-gradient-to-bl from-emerald-600 via-[#011c12] to-teal-600 rounded-lg filter blur transition duration-1000 animate-tilt"
                            }), Object(a.jsxs)("div", {
                                className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-emerald-900 via-[#00150d] to-teal-900 rounded-xl text-center lg:col-span-1 relative",
                                children: [Object(a.jsx)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-20 sm:px-32",
                                    children: "Step 5: Mint"
                                }), Object(a.jsx)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-bold leading-relax mt-0 text-transparent bg-clip-text bg-gradient-to-tr from-blue-200 to-white"
                                }), Object(a.jsx)("p", {
                                    className: "w-full text-sm tracking-wide leading-tight text-white pb-2",
                                    children: X ? Object(a.jsxs)("span", {
                                        className: "text-lg font-semibold text-green-400 text-opacity-90",
                                        children: [de, " has started"]
                                    }) : Object(a.jsx)("span", {
                                        className: "text-base text-red-500 text-opacity-70 px-6",
                                        children: "Mint Button will automatically be enabled upon mint time"
                                    })
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 text-lg tracking-wide leading-tight text-white",
                                    children: ["Price per Mint:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== ee && void 0 !== ee ? ee : "0", "ether"), " $MAD"]
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "flex flex-row h-8 w-24 rounded-lg relative",
                                    children: [Object(a.jsx)("button", {
                                        onClick: function() {
                                            G > 1 && (H(G - 1),
                                            Y(J - .1),
                                            L())
                                        },
                                        disabled: I,
                                        className: "font-semibold ".concat(I ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-l focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "-"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "bg-white w-24 text-xs md:text-base flex items-center justify-center cursor-default",
                                        children: Object(a.jsx)("span", {
                                            children: G
                                        })
                                    }), Object(a.jsx)("button", {
                                        onClick: function() {
                                            G < se && (H(G + 1),
                                            Y(J + .1),
                                            L())
                                        },
                                        disabled: I,
                                        className: "font-semibold ".concat(I ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-r focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "+"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "absolute flex flex-col p-2 w-32 md:w-full mt-6 md:mt-8 items-start justify-center"
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Total supply: ".concat(V, "/").concat(6e3), " left"]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Max mint per transaction: ".concat(se), Object(a.jsx)("br", {}), "Remaining mint for your wallet: ".concat(ne - re)]
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 pt-2 text-lg tracking-wide leading-tight text-white",
                                    children: ["Total Price:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== ue && void 0 !== ue ? ue : "0", "ether"), " $MAD"]
                                    })]
                                }), I ? Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)(S.a, {
                                        loading: !0,
                                        className: "bg-gradient-to-r from-emerald-600 to-teal-600 hover:from-emerald-800 hover:to-teal-800 text-gray-200 font-bold",
                                        onClick: function() {},
                                        children: "Transaction In Progress"
                                    }), Object(a.jsx)("br", {})]
                                }) : Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)("button", {
                                        disabled: ie,
                                        className: "opacity-95 rounded-lg px-4 title-font p-3 ".concat(ie ? "bg-gradient-to-r from-gray-800 to-gray-800 text-gray-400 cursor-not-allowed" : " text-white font-semibold bg-gradient-to-r from-emerald-600 to-teal-600 hover:from-emerald-700 hover:to-teal-700 focus:outline-none focus:ring-2 focus:ring-emerald-600 focus:ring-opacity-50"),
                                        onClick: function() {
                                            G >= 1 && G <= se ? function() {
                                                xe.apply(this, arguments)
                                            }() : Object(h.b)("Invalid Mint Amount", {
                                                autoClose: 3e4
                                            })
                                        },
                                        children: Object(a.jsx)("div", {
                                            children: "0" === V ? "Sold Out" : te ? "0" === le ? "Your wallet is not approved to deduct $MAD" : 0 === P ? "You must have a Genesis Mad Meerkat" : re === ne ? "You have reached max mint for this round" : X ? "Mint" : "You are whitelisted but ".concat(de, " has not started") : "Your wallet is not whitelisted"
                                        })
                                    }), N && Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80 pt-1",
                                        children: ["Your Wallet Address: ", null === N || void 0 === N ? void 0 : N.slice(0, 6), "...", null === N || void 0 === N ? void 0 : N.slice(-6)]
                                    }), Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80",
                                        children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (y = null === ce || void 0 === ce ? void 0 : ce.toString()) && void 0 !== y ? y : "0", "ether")).toLocaleString(void 0, {
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 4
                                        })]
                                    }), ae > 0 && Object(a.jsxs)("div", {
                                        className: "text-xs text-white pt-2 cursor-pointer hover:text-emerald-500",
                                        onClick: function() {
                                            return window.open("/gallery", "_blank")
                                        },
                                        children: ["You have ", ae, " DMM", ae > 1 ? "s" : "", ". View them in the Gallery."]
                                    })]
                                })]
                            })]
                        })
                    })]
                })]
            })
        };
        function Q() {
            function e() {
                return (e = Object(x.a)(d.a.mark((function e() {
                    var t, s, a;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (t = window.ethereum,
                                s = [{
                                    chainId: g.a,
                                    chainName: "Cronos",
                                    nativeCurrency: {
                                        name: "CRO",
                                        symbol: "CRO",
                                        decimals: 18
                                    },
                                    rpcUrls: [g.b],
                                    blockExplorerUrls: ["https://cronos.crypto.org/explorer/"]
                                }],
                                !t || !t.request) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 3,
                                e.next = 6,
                                t.request({
                                    method: "wallet_switchEthereumChain",
                                    params: [{
                                        chainId: g.a
                                    }]
                                }).catch();
                            case 6:
                                (a = e.sent) && console.log(a),
                                e.next = 21;
                                break;
                            case 10:
                                if (e.prev = 10,
                                e.t0 = e.catch(3),
                                4902 !== e.t0.code) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 13,
                                e.next = 16,
                                t.request({
                                    method: "wallet_addEthereumChain",
                                    params: s
                                });
                            case 16:
                                e.next = 21;
                                break;
                            case 18:
                                e.prev = 18,
                                e.t1 = e.catch(13),
                                console.log(e.t1);
                            case 21:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[3, 10], [13, 18]])
                }
                )))).apply(this, arguments)
            }
            Object(n.useEffect)((function() {
                !function() {
                    e.apply(this, arguments)
                }()
            }
            ), []);
            var t = Object(n.useState)(!1)
              , s = t[0]
              , l = t[1]
              , c = Object(n.useState)(!1)[0]
              , r = Object(j.a)()
              , i = Object(k.b)()
              , o = i.account
              , u = i.library
              , b = i.active
              , h = i.chainId;
            return Object(a.jsx)(a.Fragment, {
                children: b && h ? Object(a.jsx)(w.a, {
                    value: {
                        web3Provider: u,
                        ABIs: new Map([[g.d, m], [g.f, p]]),
                        refreshInterval: 8e3
                    },
                    children: Object(a.jsx)(P, {})
                }) : Object(a.jsxs)("div", {
                    className: "flex justify-center my-12",
                    children: [Object(a.jsx)(S.a, {
                        block: !1,
                        type: "primary",
                        disabled: c,
                        onClick: Object(x.a)(d.a.mark((function e() {
                            return d.a.wrap((function(e) {
                                for (; ; )
                                    switch (e.prev = e.next) {
                                    case 0:
                                        !o && l(!0);
                                    case 1:
                                    case "end":
                                        return e.stop()
                                    }
                            }
                            ), e)
                        }
                        ))),
                        children: r ? o ? "".concat(o.slice(0, 6), "...\n                ").concat(o.slice(o.length - 4, o.length)) : Object(a.jsx)("div", {
                            className: "flex items-center text-xl justify-center text-white",
                            children: Object(a.jsx)("span", {
                                children: "Connect Your Wallet"
                            })
                        }) : "Loading..."
                    }), Object(a.jsx)(A.a, {
                        visible: s,
                        onClose: function() {
                            return l(!1)
                        }
                    })]
                })
            })
        }
        v.a.extend(y.a);
        var F = function() {
            var e, t, s, l, c, r, i, o, j, f, v, O, y, N, A, z, P, Q = Object(k.b)().account, F = Object(n.useState)(!1), W = F[0], D = F[1], T = Object(b.a)(g.f, p);
            function q() {
                return (q = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!Q) {
                                    e.next = 14;
                                    break
                                }
                                return D(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === T || void 0 === T || null === (t = T.methods) || void 0 === t ? void 0 : t.approve(g.d, g.g).send({
                                    from: Q,
                                    value: "0"
                                });
                            case 5:
                                Object(h.b)("You have successfully approved $MAD usage!", {
                                    autoClose: 3e4
                                }),
                                e.next = 11;
                                break;
                            case 8:
                                e.prev = 8,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 11:
                                D(!1),
                                e.next = 15;
                                break;
                            case 14:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 15:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 8]])
                }
                )))).apply(this, arguments)
            }
            var R = Object(n.useState)(.75)
              , E = R[0]
              , J = R[1]
              , Y = Object(n.useState)(!1)
              , _ = Y[0]
              , I = Y[1]
              , U = Object(C.a)("/sounds/glug.wav", {
                playbackRate: E,
                volume: .3
            })
              , Z = Object(u.a)(U, 1)[0]
              , L = Object(n.useState)(1)
              , K = L[0]
              , G = L[1]
              , H = Object(w.b)([[g.d, "presale2Active"], [g.d, "presale1reservedDegens"], [g.d, "degenPricePresale2"], [g.d, "isWhitelist2Manual", Q], [g.d, "MAX_DEGEN_PURCHASE"], [g.d, "balanceOf", Q], [g.d, "MAX_DEGEN_PER_ROUND"], [g.f, "allowance", Q, g.d], [g.f, "balanceOf", Q], [g.k, "balanceOf", Q], [g.j, "balanceOf", Q], [g.d, "amountPurchased2", Q]]).data
              , $ = null !== (e = null === H || void 0 === H ? void 0 : H[0]) && void 0 !== e && e
              , X = null !== (t = null === H || void 0 === H ? void 0 : H[1].toString()) && void 0 !== t ? t : 0
              , V = null !== (s = null === H || void 0 === H ? void 0 : H[2].toString()) && void 0 !== s ? s : "55000000000000000000"
              , ee = null !== (l = null === H || void 0 === H ? void 0 : H[3]) && void 0 !== l && l
              , te = parseFloat(null !== (c = null === H || void 0 === H ? void 0 : H[4].toString()) && void 0 !== c ? c : 0)
              , se = parseInt(null !== (r = null === H || void 0 === H ? void 0 : H[5].toString()) && void 0 !== r ? r : 0)
              , ae = parseFloat(null !== (i = null === H || void 0 === H ? void 0 : H[6].toString()) && void 0 !== i ? i : 0)
              , ne = null === H || void 0 === H || null === (o = H[7]) || void 0 === o ? void 0 : o.toString()
              , le = null !== (j = null === H || void 0 === H ? void 0 : H[8]) && void 0 !== j ? j : 0
              , ce = parseInt(null !== (f = null === H || void 0 === H || null === (v = H[9]) || void 0 === v ? void 0 : v.toString()) && void 0 !== f ? f : 0)
              , re = parseInt(null !== (O = null === H || void 0 === H || null === (y = H[10]) || void 0 === y ? void 0 : y.toString()) && void 0 !== O ? O : 0)
              , ie = parseFloat(null !== (N = null === H || void 0 === H ? void 0 : H[11].toString()) && void 0 !== N ? N : 0)
              , oe = !ee || !$ || "0" === X || "0" === ne || 0 === ce && 0 === re || ie === ae
              , de = Object(b.a)(g.d, m)
              , ue = "CRO Round"
              , xe = (parseInt(V) * K).toLocaleString("fullwide", {
                useGrouping: !1
            });
            function me() {
                return (me = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!Q) {
                                    e.next = 17;
                                    break
                                }
                                return I(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === de || void 0 === de ? void 0 : de.methods.presalemintDegen2(K, xe).estimateGas({
                                    from: Q,
                                    value: "0"
                                });
                            case 5:
                                return t = e.sent,
                                e.next = 8,
                                null === de || void 0 === de ? void 0 : de.methods.presalemintDegen2(K, xe).send({
                                    from: Q,
                                    value: "0",
                                    gasLimit: Math.trunc(1.4 * t),
                                    gasPrice: 8e12
                                });
                            case 8:
                                Object(h.b)("You have successfully minted ".concat(K, " DMM!"), {
                                    autoClose: 3e4
                                }),
                                e.next = 14;
                                break;
                            case 11:
                                e.prev = 11,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 14:
                                I(!1),
                                e.next = 18;
                                break;
                            case 17:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 18:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 11]])
                }
                )))).apply(this, arguments)
            }
            return Object(a.jsxs)(a.Fragment, {
                children: [Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "text-sm text-gray-200 flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 text-center lg:col-span-1 relative bg-gradient-to-b from-purple-900 via-[#170116] to-pink-900 rounded-lg p-2 cursor-default",
                        children: ["Connected to ", null === Q || void 0 === Q ? void 0 : Q.slice(0, 4), "...", null === Q || void 0 === Q ? void 0 : Q.slice(-4)]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-purple-900 via-[#170116] to-pink-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 1: Secure Whitelist Snapshot"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4",
                            children: ["Ensure either a CRO Mad Meerkat or Treehouse", Object(a.jsx)("br", {}), "is held in this wallet from ", Object(a.jsx)("br", {}), "Saturday 14th May 2022 9AM UTC onwards"]
                        }), Object(a.jsxs)("p", {
                            className: "pt-8 text-2xl text-gray-300 px-4",
                            children: ["You have ", re, " Cro Mad Meerkat", re > 0 ? "s" : "", " & ", ce, " Treehouse", ce > 0 ? "s" : ""]
                        }), Object(a.jsxs)("p", {
                            className: "text-base text-gray-400 px-4 pb-4",
                            children: ["Adopt a Cro Mad Meerkat", " ", Object(a.jsx)("a", {
                                href: "/adoption",
                                target: "_blank",
                                className: "text-pink-500 hover:text-pink-200 cursor-pointer",
                                children: "here"
                            }), " ", Object(a.jsx)("br", {}), " Get a Mad Meerkat Treehouse", " ", Object(a.jsx)("a", {
                                href: "/treehouse",
                                target: "_blank",
                                className: "text-pink-500 hover:text-pink-200 cursor-pointer",
                                children: "here"
                            }), " "]
                        }), ee ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsx)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: "You have secured a whitelist spot!"
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["Checkmark will turn green ", Object(a.jsx)("br", {}), "once your wallet is whitelisted"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-purple-900 via-[#170116] to-pink-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 mx-8",
                            children: "Step 2: Load up $MAD Token"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Maximum of 5 DMM can be minted if you are whitelisted"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Each DMM will cost 55 $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "pt-8 text-2xl text-gray-300 px-4",
                            children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (A = null === le || void 0 === le ? void 0 : le.toString()) && void 0 !== A ? A : "0", "ether")).toLocaleString(void 0, {
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 4
                            }), " ", Object(a.jsx)("div", {
                                className: "text-xs text-gray-400",
                                children: "Keep some CRO for transaction fees"
                            })]
                        }), Object(a.jsxs)("p", {
                            className: "text-base text-gray-400 px-4 pb-4",
                            children: ["Get more $MAD by staking Treehouses on", " ", Object(a.jsx)("a", {
                                href: "https://mmtreehouse.io",
                                target: "_blank",
                                className: "text-pink-500 hover:text-pink-200 cursor-pointer",
                                children: "MM Treehouse"
                            }), " ", Object(a.jsx)("br", {}), " or purchase from", " ", Object(a.jsx)("a", {
                                href: "https://mm.finance/swap?outputCurrency=0x212331e1435A8df230715dB4C02B2a3A0abF8c61",
                                target: "_blank",
                                className: "text-pink-500 hover:text-pink-200 cursor-pointer",
                                children: "MM.Finance"
                            })]
                        }), parseFloat(M.a.utils.fromWei(null !== (z = null === le || void 0 === le ? void 0 : le.toString()) && void 0 !== z ? z : "0", "ether")) >= 55 ? Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " "]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsx)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: "Get More MAD!"
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-purple-900 via-[#170116] to-pink-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 3: Approve usage of $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4 pb-4",
                            children: ["One Time Approval required ", Object(a.jsx)("br", {}), " to allow deduction of $MAD during minting"]
                        }), "0" !== ne ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsx)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: "Wallet Approved!"
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("button", {
                                disabled: W,
                                className: "text-white rounded-xl p-2 bg-gradient-to-r from-purple-600 to-pink-600 hover:from-purple-700 hover:to-pink-700",
                                onClick: function() {
                                    !function() {
                                        q.apply(this, arguments)
                                    }()
                                },
                                children: W ? Object(a.jsx)("div", {
                                    className: "pl-8 pr-4",
                                    children: Object(a.jsx)(B.a, {})
                                }) : "Approve Usage"
                            }), Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40 pt-4"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["Checkmark will turn green ", Object(a.jsx)("br", {}), "once your wallet has approved"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-purple-900 via-[#170116] to-pink-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 4: Hold!"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4 pb-4",
                            children: ["Cro Mad Meerkat or Treehouse must be", Object(a.jsx)("br", {}), "in the wallet at the point of minting"]
                        }), re > 0 || ce > 0 ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsxs)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: ["You have ", re, " Cro Mad Meerkat", re > 0 ? "s" : "", " ", "& ", ce, " Treehouse", ce > 0 ? "s" : ""]
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["You have ", re, " Cro Mad Meerkat", re > 0 ? "s" : "", " ", "& ", ce, " Treehouse", ce > 0 ? "s" : ""]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsxs)("div", {
                    className: "grid grid-cols-1 m-4",
                    children: [Object(a.jsx)("div", {
                        className: "lg:block xs:hidden flex place-self-center"
                    }), Object(a.jsx)("div", {
                        className: "grid gap-8 items-start justify-center p-4",
                        children: Object(a.jsxs)("div", {
                            className: "relative",
                            children: [Object(a.jsx)("div", {
                                className: "absolute -inset-2 bg-gradient-to-bl from-purple-600 via-[#2a0228] to-pink-600 rounded-lg filter blur transition duration-1000 animate-tilt"
                            }), Object(a.jsxs)("div", {
                                className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-purple-900 via-[#170116] to-pink-900 rounded-xl text-center lg:col-span-1 relative",
                                children: [Object(a.jsx)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-20 sm:px-32",
                                    children: "Step 5: Mint"
                                }), Object(a.jsx)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-bold leading-relax mt-0 text-transparent bg-clip-text bg-gradient-to-tr from-blue-200 to-white"
                                }), Object(a.jsx)("p", {
                                    className: "w-full text-sm tracking-wide leading-tight text-white pb-2",
                                    children: $ ? Object(a.jsxs)("span", {
                                        className: "text-lg font-semibold text-green-400 text-opacity-90",
                                        children: [ue, " has started"]
                                    }) : Object(a.jsx)("span", {
                                        className: "text-base text-red-500 text-opacity-70 px-6",
                                        children: "Mint Button will automatically be enabled upon mint time"
                                    })
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 text-lg tracking-wide leading-tight text-white",
                                    children: ["Price per Mint:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== V && void 0 !== V ? V : "0", "ether"), " $MAD"]
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "flex flex-row h-8 w-24 rounded-lg relative",
                                    children: [Object(a.jsx)("button", {
                                        onClick: function() {
                                            K > 1 && (G(K - 1),
                                            J(E - .1),
                                            Z())
                                        },
                                        disabled: _,
                                        className: "font-semibold ".concat(_ ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-l focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "-"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "bg-white w-24 text-xs md:text-base flex items-center justify-center cursor-default",
                                        children: Object(a.jsx)("span", {
                                            children: K
                                        })
                                    }), Object(a.jsx)("button", {
                                        onClick: function() {
                                            K < te && (G(K + 1),
                                            J(E + .1),
                                            Z())
                                        },
                                        disabled: _,
                                        className: "font-semibold ".concat(_ ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-r focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "+"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "absolute flex flex-col p-2 w-32 md:w-full mt-6 md:mt-8 items-start justify-center"
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Total supply: ".concat(X, "/").concat(6e3), " left"]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Max mint per transaction: ".concat(te), Object(a.jsx)("br", {}), "Remaining mint for your wallet: ".concat(ae - ie)]
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 pt-2 text-lg tracking-wide leading-tight text-white",
                                    children: ["Total Price:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== xe && void 0 !== xe ? xe : "0", "ether"), " $MAD"]
                                    })]
                                }), _ ? Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)(S.a, {
                                        loading: !0,
                                        className: "bg-gradient-to-r from-purple-800 to-pink-800 text-gray-200 font-bold",
                                        onClick: function() {},
                                        children: "Transaction In Progress"
                                    }), Object(a.jsx)("br", {})]
                                }) : Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)("button", {
                                        disabled: oe,
                                        className: "opacity-95 rounded-lg px-4 title-font p-3 ".concat(oe ? "bg-gradient-to-r from-gray-800 to-gray-800 text-gray-400 cursor-not-allowed" : " text-white font-semibold bg-gradient-to-r from-purple-600 to-pink-600 hover:from-purple-700 hover:to-pink-700 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50"),
                                        onClick: function() {
                                            K >= 1 && K <= te ? function() {
                                                me.apply(this, arguments)
                                            }() : Object(h.b)("Invalid Mint Amount", {
                                                autoClose: 3e4
                                            })
                                        },
                                        children: Object(a.jsx)("div", {
                                            children: "0" === X ? "Sold Out" : ee ? "0" === ne ? "Your wallet is not approved to deduct $MAD" : 0 === ce && 0 === re ? "You must either have a MMB or MMT" : ie === ae ? "You have reached max mint for this round" : $ ? "Mint" : "You are whitelisted but ".concat(ue, " has not started") : "Your wallet is not whitelisted"
                                        })
                                    }), Q && Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80 pt-1",
                                        children: ["Your Wallet Address: ", null === Q || void 0 === Q ? void 0 : Q.slice(0, 6), "...", null === Q || void 0 === Q ? void 0 : Q.slice(-6)]
                                    }), Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80",
                                        children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (P = null === le || void 0 === le ? void 0 : le.toString()) && void 0 !== P ? P : "0", "ether")).toLocaleString(void 0, {
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 4
                                        })]
                                    }), se > 0 && Object(a.jsxs)("div", {
                                        className: "text-xs text-white pt-2 cursor-pointer hover:text-pink-500",
                                        onClick: function() {
                                            return window.open("/gallery", "_blank")
                                        },
                                        children: ["You have ", se, " DMM", se > 1 ? "s" : "", ". View them in the Gallery."]
                                    })]
                                })]
                            })]
                        })
                    })]
                })]
            })
        };
        function W() {
            function e() {
                return (e = Object(x.a)(d.a.mark((function e() {
                    var t, s, a;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (t = window.ethereum,
                                s = [{
                                    chainId: g.a,
                                    chainName: "Cronos",
                                    nativeCurrency: {
                                        name: "CRO",
                                        symbol: "CRO",
                                        decimals: 18
                                    },
                                    rpcUrls: [g.b],
                                    blockExplorerUrls: ["https://cronos.crypto.org/explorer/"]
                                }],
                                !t || !t.request) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 3,
                                e.next = 6,
                                t.request({
                                    method: "wallet_switchEthereumChain",
                                    params: [{
                                        chainId: g.a
                                    }]
                                }).catch();
                            case 6:
                                (a = e.sent) && console.log(a),
                                e.next = 21;
                                break;
                            case 10:
                                if (e.prev = 10,
                                e.t0 = e.catch(3),
                                4902 !== e.t0.code) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 13,
                                e.next = 16,
                                t.request({
                                    method: "wallet_addEthereumChain",
                                    params: s
                                });
                            case 16:
                                e.next = 21;
                                break;
                            case 18:
                                e.prev = 18,
                                e.t1 = e.catch(13),
                                console.log(e.t1);
                            case 21:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[3, 10], [13, 18]])
                }
                )))).apply(this, arguments)
            }
            Object(n.useEffect)((function() {
                !function() {
                    e.apply(this, arguments)
                }()
            }
            ), []);
            var t = Object(n.useState)(!1)
              , s = t[0]
              , l = t[1]
              , c = Object(n.useState)(!1)[0]
              , r = Object(j.a)()
              , i = Object(k.b)()
              , o = i.account
              , u = i.library
              , b = i.active
              , h = i.chainId;
            return Object(a.jsx)(a.Fragment, {
                children: b && h ? Object(a.jsx)(w.a, {
                    value: {
                        web3Provider: u,
                        ABIs: new Map([[g.d, m], [g.k, m], [g.j, m], [g.f, p]]),
                        refreshInterval: 8e3
                    },
                    children: Object(a.jsx)(F, {})
                }) : Object(a.jsxs)("div", {
                    className: "flex justify-center my-12",
                    children: [Object(a.jsx)(S.a, {
                        block: !1,
                        type: "primary",
                        disabled: c,
                        onClick: Object(x.a)(d.a.mark((function e() {
                            return d.a.wrap((function(e) {
                                for (; ; )
                                    switch (e.prev = e.next) {
                                    case 0:
                                        !o && l(!0);
                                    case 1:
                                    case "end":
                                        return e.stop()
                                    }
                            }
                            ), e)
                        }
                        ))),
                        children: r ? o ? "".concat(o.slice(0, 6), "...\n                ").concat(o.slice(o.length - 4, o.length)) : Object(a.jsx)("div", {
                            className: "flex items-center text-xl justify-center text-white",
                            children: Object(a.jsx)("span", {
                                children: "Connect Your Wallet"
                            })
                        }) : "Loading..."
                    }), Object(a.jsx)(A.a, {
                        visible: s,
                        onClose: function() {
                            return l(!1)
                        }
                    })]
                })
            })
        }
        v.a.extend(y.a);
        var D = function() {
            var e, t, s, l, c, r, i, o, j, f, v, O, y, N = Object(k.b)().account, A = Object(n.useState)(!1), z = A[0], P = A[1], Q = Object(b.a)(g.f, p);
            function F() {
                return (F = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!N) {
                                    e.next = 14;
                                    break
                                }
                                return P(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === Q || void 0 === Q || null === (t = Q.methods) || void 0 === t ? void 0 : t.approve(g.d, g.g).send({
                                    from: N,
                                    value: "0"
                                });
                            case 5:
                                Object(h.b)("You have successfully approved $MAD usage!", {
                                    autoClose: 3e4
                                }),
                                e.next = 11;
                                break;
                            case 8:
                                e.prev = 8,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 11:
                                P(!1),
                                e.next = 15;
                                break;
                            case 14:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 15:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 8]])
                }
                )))).apply(this, arguments)
            }
            var W = Object(n.useState)(.75)
              , D = W[0]
              , T = W[1]
              , q = Object(n.useState)(!1)
              , R = q[0]
              , E = q[1]
              , J = Object(C.a)("/sounds/glug.wav", {
                playbackRate: D,
                volume: .3
            })
              , Y = Object(u.a)(J, 1)[0]
              , _ = Object(n.useState)(1)
              , I = _[0]
              , U = _[1]
              , Z = Object(w.b)([[g.d, "saleIsActive"], [g.d, "totalSupply"], [g.d, "degenPrice"], [g.d, "MAX_DEGEN_PURCHASE"], [g.d, "presale1reservedDegens"], [g.d, "balanceOf", N], [g.d, "MAX_DEGEN_PER_ROUND"], [g.f, "balanceOf", N], [g.f, "allowance", N, g.d], [g.d, "amountPurchasedPublic", N]]).data
              , L = null !== (e = null === Z || void 0 === Z ? void 0 : Z[0]) && void 0 !== e && e
              , K = null !== (t = null === Z || void 0 === Z ? void 0 : Z[1].toString()) && void 0 !== t ? t : 0
              , G = null !== (s = null === Z || void 0 === Z ? void 0 : Z[2].toString()) && void 0 !== s ? s : "75000000000000000000"
              , H = parseFloat(null !== (l = null === Z || void 0 === Z ? void 0 : Z[3].toString()) && void 0 !== l ? l : 0)
              , $ = null !== (c = null === Z || void 0 === Z ? void 0 : Z[4].toString()) && void 0 !== c ? c : 0
              , X = parseInt(null !== (r = null === Z || void 0 === Z ? void 0 : Z[5].toString()) && void 0 !== r ? r : 0)
              , V = parseFloat(null !== (i = null === Z || void 0 === Z ? void 0 : Z[6].toString()) && void 0 !== i ? i : 0)
              , ee = null !== (o = null === Z || void 0 === Z ? void 0 : Z[7]) && void 0 !== o ? o : 0
              , te = null === Z || void 0 === Z || null === (j = Z[8]) || void 0 === j ? void 0 : j.toString()
              , se = parseFloat(null !== (f = null === Z || void 0 === Z ? void 0 : Z[9].toString()) && void 0 !== f ? f : 0)
              , ae = (12e3 - (parseInt(K) + parseInt($))).toString()
              , ne = !L || parseInt(ae) < 1 || "0" === te || se === V
              , le = Object(b.a)(g.d, m)
              , ce = "Public Sale"
              , re = (parseInt(G) * I).toLocaleString("fullwide", {
                useGrouping: !1
            });
            function ie() {
                return (ie = Object(x.a)(d.a.mark((function e() {
                    var t;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (!N) {
                                    e.next = 17;
                                    break
                                }
                                return E(!0),
                                e.prev = 2,
                                e.next = 5,
                                null === le || void 0 === le ? void 0 : le.methods.mintDegen(I, re).estimateGas({
                                    from: N,
                                    value: "0"
                                });
                            case 5:
                                return t = e.sent,
                                e.next = 8,
                                null === le || void 0 === le ? void 0 : le.methods.mintDegen(I, re).send({
                                    from: N,
                                    value: "0",
                                    gasLimit: Math.trunc(1.4 * t),
                                    gasPrice: 8e12
                                });
                            case 8:
                                Object(h.b)("You have successfully minted ".concat(I, " DMM!"), {
                                    autoClose: 3e4
                                }),
                                e.next = 14;
                                break;
                            case 11:
                                e.prev = 11,
                                e.t0 = e.catch(2),
                                "MetaMask Tx Signature: User denied transaction signature." === (null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message) ? Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                }) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("insufficient balance") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Insufficient Balance", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("User canceled") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("You have rejected the transaction.", {
                                    autoClose: 3e4
                                })) : null !== e.t0 && void 0 !== e.t0 && e.t0.message.includes("The operation couldn\u2019t be completed") ? (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Please ensure that you have sufficient balance in Cronos. You may need to bridge your CRO from Crypto.org Mainnet to Cronos Beta in your DeFi Wallet App.", {
                                    autoClose: 3e4
                                })) : (console.log(null === e.t0 || void 0 === e.t0 ? void 0 : e.t0.message),
                                Object(h.b)("Execution Reverted", {
                                    autoClose: 3e4
                                }));
                            case 14:
                                E(!1),
                                e.next = 18;
                                break;
                            case 17:
                                Object(h.b)("Wallet is not connected.", {
                                    autoClose: 3e4
                                });
                            case 18:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[2, 11]])
                }
                )))).apply(this, arguments)
            }
            return Object(a.jsxs)(a.Fragment, {
                children: [Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "text-sm text-gray-200 flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 text-center lg:col-span-1 relative bg-gradient-to-b from-blue-900 via-[#08011f] to-sky-900 rounded-lg p-2 cursor-default",
                        children: ["Connected to ", null === N || void 0 === N ? void 0 : N.slice(0, 4), "...", null === N || void 0 === N ? void 0 : N.slice(-4)]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center p-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-blue-900 via-[#08011f] to-sky-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 mx-8",
                            children: "Step 1: Load up $MAD Token"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Maximum of 5 DMM can be minted per wallet"
                        }), Object(a.jsx)("p", {
                            className: "text-base text-gray-400 px-4",
                            children: "Each DMM will cost 75 $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "pt-8 text-2xl text-gray-300 px-4",
                            children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (v = null === ee || void 0 === ee ? void 0 : ee.toString()) && void 0 !== v ? v : "0", "ether")).toLocaleString(void 0, {
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 4
                            }), " ", Object(a.jsx)("div", {
                                className: "text-xs text-gray-400",
                                children: "Keep some CRO for transaction fees"
                            })]
                        }), Object(a.jsxs)("p", {
                            className: "text-base text-gray-400 px-4 pb-4",
                            children: ["Get more $MAD by staking Treehouses on", " ", Object(a.jsx)("a", {
                                href: "https://mmtreehouse.io",
                                target: "_blank",
                                className: "text-blue-500 hover:text-blue-200 cursor-pointer",
                                children: "MM Treehouse"
                            }), " ", Object(a.jsx)("br", {}), " or purchase from", " ", Object(a.jsx)("a", {
                                href: "https://mm.finance/swap?outputCurrency=0x212331e1435A8df230715dB4C02B2a3A0abF8c61",
                                target: "_blank",
                                className: "text-blue-500 hover:text-blue-200 cursor-pointer",
                                children: "MM.Finance"
                            })]
                        }), parseFloat(M.a.utils.fromWei(null !== (O = null === ee || void 0 === ee ? void 0 : ee.toString()) && void 0 !== O ? O : "0", "ether")) >= 55 ? Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " "]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40"
                            }), Object(a.jsx)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: "Get More MAD!"
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center pb-4 pt-8 px-4",
                    children: Object(a.jsxs)("div", {
                        className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-blue-900 via-[#08011f] to-sky-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                        children: [Object(a.jsx)("h1", {
                            className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2",
                            children: "Step 2: Approve usage of $MAD"
                        }), Object(a.jsxs)("p", {
                            className: "w-full text-base tracking-wide leading-tight text-gray-400 px-4 pb-4",
                            children: ["One Time Approval required ", Object(a.jsx)("br", {}), " to allow deduction of $MAD during minting"]
                        }), "0" !== te ? Object(a.jsxs)(a.Fragment, {
                            children: [" ", Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-green.svg",
                                className: "w-24"
                            }), " ", Object(a.jsx)("p", {
                                className: "text-base text-gray-200 px-4",
                                children: "Wallet Approved!"
                            })]
                        }) : Object(a.jsxs)(a.Fragment, {
                            children: [Object(a.jsx)("button", {
                                disabled: z,
                                className: "text-white rounded-xl p-2 bg-gradient-to-r from-blue-900 to-sky-700 hover:from-blue-800 hover:to-sky-600",
                                onClick: function() {
                                    !function() {
                                        F.apply(this, arguments)
                                    }()
                                },
                                children: z ? Object(a.jsx)("div", {
                                    className: "pl-8 pr-4",
                                    children: Object(a.jsx)(B.a, {})
                                }) : "Approve Usage"
                            }), Object(a.jsx)("img", {
                                src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/check-grey.svg",
                                className: "w-24 opacity-40 pt-4"
                            }), Object(a.jsxs)("p", {
                                className: "text-base text-gray-400 px-4",
                                children: ["Checkmark will turn green ", Object(a.jsx)("br", {}), "once your wallet has approved"]
                            })]
                        })]
                    })
                }), Object(a.jsx)("div", {
                    className: "grid justify-center py-8",
                    children: Object(a.jsx)("img", {
                        src: "https://madmeerkat.mypinata.cloud/ipfs/QmQzzqWwQgBFAuJdWkZ1pRsjJxqBno7gt2BedtTHSzPPKz/arrow.svg",
                        className: "w-12 rotate-90 animate-pulse"
                    })
                }), Object(a.jsxs)("div", {
                    className: "grid grid-cols-1 m-4",
                    children: [Object(a.jsx)("div", {
                        className: "lg:block xs:hidden flex place-self-center"
                    }), Object(a.jsx)("div", {
                        className: "grid gap-8 items-start justify-center p-4",
                        children: Object(a.jsxs)("div", {
                            className: "relative",
                            children: [Object(a.jsx)("div", {
                                className: "absolute -inset-2 bg-gradient-to-bl from-blue-600 via-[#0d0330] to-sky-600 rounded-lg filter blur transition duration-1000 animate-tilt"
                            }), Object(a.jsxs)("div", {
                                className: "flex flex-col space-y-2 items-center flex-1 h-full xs:px-4 px-24 py-8 bg-gradient-to-bl from-blue-900 via-[#08011f] to-sky-900 bg-opacity-90 rounded-xl text-center lg:col-span-1 relative",
                                children: [Object(a.jsxs)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-semibold leading-relax mt-4 text-gray-100 px-2 sm:px-12",
                                    children: ["Step 3: Mint ", Object(a.jsx)("br", {}), Object(a.jsxs)("span", {
                                        className: "text-base",
                                        children: ["Begin on Monday 16th May 2022 1PM UTC", Object(a.jsx)("br", {}), "(Should there be any remaining from Genesis and Cro Round)", " "]
                                    })]
                                }), Object(a.jsx)("h1", {
                                    className: "xs:text-2xl sm:text-2xl lg:text-3xl font-bold leading-relax mt-0 text-transparent bg-clip-text bg-gradient-to-tr from-blue-200 to-white"
                                }), Object(a.jsx)("p", {
                                    className: "w-full text-sm tracking-wide leading-tight text-white pb-2",
                                    children: L ? Object(a.jsxs)("span", {
                                        className: "text-lg font-semibold text-green-400 text-opacity-90",
                                        children: [ce, " has started"]
                                    }) : Object(a.jsx)("span", {
                                        className: "text-base text-red-500 text-opacity-70 px-6",
                                        children: "Mint Button will automatically be enabled upon mint time"
                                    })
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 text-lg tracking-wide leading-tight text-white",
                                    children: ["Price per Mint:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== G && void 0 !== G ? G : "0", "ether"), " $MAD"]
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "flex flex-row h-8 w-24 rounded-lg relative",
                                    children: [Object(a.jsx)("button", {
                                        onClick: function() {
                                            I > 1 && (U(I - 1),
                                            T(D - .1),
                                            Y())
                                        },
                                        disabled: R,
                                        className: "font-semibold ".concat(R ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-l focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "-"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "bg-white w-24 text-xs md:text-base flex items-center justify-center cursor-default",
                                        children: Object(a.jsx)("span", {
                                            children: I
                                        })
                                    }), Object(a.jsx)("button", {
                                        onClick: function() {
                                            I < H && (U(I + 1),
                                            T(D + .1),
                                            Y())
                                        },
                                        disabled: R,
                                        className: "font-semibold ".concat(R ? "bg-gray-800" : "bg-[#88bf91] hover:bg-[#6d9c75]", " text-white h-full w-20 flex rounded-r focus:outline-none cursor-pointer"),
                                        children: Object(a.jsx)("span", {
                                            className: "m-auto",
                                            children: "+"
                                        })
                                    }), Object(a.jsx)("div", {
                                        className: "absolute flex flex-col p-2 w-32 md:w-full mt-6 md:mt-8 items-start justify-center"
                                    })]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Total supply: ".concat(parseInt(ae) < 1 ? 0 : ae, "/").concat(6e3), " ", "left"]
                                }), Object(a.jsxs)("div", {
                                    className: "text-sm text-gray-400",
                                    children: ["Max mint per transaction: ".concat(H), Object(a.jsx)("br", {}), "Remaining mint for your wallet: ".concat(V - se)]
                                }), Object(a.jsxs)("p", {
                                    className: "w-full pb-4 pt-2 text-lg tracking-wide leading-tight text-white",
                                    children: ["Total Price:", " ", Object(a.jsxs)("span", {
                                        className: "font-semibold",
                                        children: [M.a.utils.fromWei(null !== re && void 0 !== re ? re : "0", "ether"), " $MAD"]
                                    })]
                                }), R ? Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)(S.a, {
                                        loading: !0,
                                        className: "bg-gradient-to-r from-blue-600 to-sky-600 hover:from-blue-800 hover:to-sky-800 text-gray-200 font-bold",
                                        onClick: function() {},
                                        children: "Transaction In Progress"
                                    }), Object(a.jsx)("br", {})]
                                }) : Object(a.jsxs)(a.Fragment, {
                                    children: [Object(a.jsx)("button", {
                                        disabled: ne,
                                        className: "opacity-95 rounded-lg px-4 title-font p-3 ".concat(ne ? "bg-gradient-to-r from-gray-800 to-gray-800 text-gray-400 cursor-not-allowed" : " text-white font-semibold bg-gradient-to-r from-blue-600 to-sky-600 hover:from-blue-700 hover:to-sky-700 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:ring-opacity-50"),
                                        onClick: function() {
                                            I >= 1 && I <= H ? function() {
                                                ie.apply(this, arguments)
                                            }() : Object(h.b)("Invalid Mint Amount", {
                                                autoClose: 3e4
                                            })
                                        },
                                        children: Object(a.jsx)("div", {
                                            children: "0" === ae ? "Sold Out" : "0" === te ? "Your wallet is not approved to deduct $MAD" : se === V ? "You have reached max mint for this round" : L ? "Mint" : "".concat(ce, " has not started")
                                        })
                                    }), N && Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80 pt-1",
                                        children: ["Your Wallet Address: ", null === N || void 0 === N ? void 0 : N.slice(0, 6), "...", null === N || void 0 === N ? void 0 : N.slice(-6)]
                                    }), Object(a.jsxs)("div", {
                                        className: "text-xs text-white opacity-80",
                                        children: ["Your $MAD Balance:", " ", parseFloat(M.a.utils.fromWei(null !== (y = null === ee || void 0 === ee ? void 0 : ee.toString()) && void 0 !== y ? y : "0", "ether")).toLocaleString(void 0, {
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 4
                                        })]
                                    }), X > 0 && Object(a.jsxs)("div", {
                                        className: "text-xs text-white pt-2 cursor-pointer hover:text-blue-500",
                                        onClick: function() {
                                            return window.open("/gallery", "_blank")
                                        },
                                        children: ["You have ", X, " DMM", X > 1 ? "s" : "", ". View them in the Gallery."]
                                    })]
                                })]
                            })]
                        })
                    })]
                })]
            })
        };
        function T() {
            function e() {
                return (e = Object(x.a)(d.a.mark((function e() {
                    var t, s, a;
                    return d.a.wrap((function(e) {
                        for (; ; )
                            switch (e.prev = e.next) {
                            case 0:
                                if (t = window.ethereum,
                                s = [{
                                    chainId: g.a,
                                    chainName: "Cronos",
                                    nativeCurrency: {
                                        name: "CRO",
                                        symbol: "CRO",
                                        decimals: 18
                                    },
                                    rpcUrls: [g.b],
                                    blockExplorerUrls: ["https://cronos.crypto.org/explorer/"]
                                }],
                                !t || !t.request) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 3,
                                e.next = 6,
                                t.request({
                                    method: "wallet_switchEthereumChain",
                                    params: [{
                                        chainId: g.a
                                    }]
                                }).catch();
                            case 6:
                                (a = e.sent) && console.log(a),
                                e.next = 21;
                                break;
                            case 10:
                                if (e.prev = 10,
                                e.t0 = e.catch(3),
                                4902 !== e.t0.code) {
                                    e.next = 21;
                                    break
                                }
                                return e.prev = 13,
                                e.next = 16,
                                t.request({
                                    method: "wallet_addEthereumChain",
                                    params: s
                                });
                            case 16:
                                e.next = 21;
                                break;
                            case 18:
                                e.prev = 18,
                                e.t1 = e.catch(13),
                                console.log(e.t1);
                            case 21:
                            case "end":
                                return e.stop()
                            }
                    }
                    ), e, null, [[3, 10], [13, 18]])
                }
                )))).apply(this, arguments)
            }
            Object(n.useEffect)((function() {
                !function() {
                    e.apply(this, arguments)
                }()
            }
            ), []);
            var t = Object(n.useState)(!1)
              , s = t[0]
              , l = t[1]
              , c = Object(n.useState)(!1)[0]
              , r = Object(j.a)()
              , i = Object(k.b)()
              , o = i.account
              , u = i.library
              , b = i.active
              , h = i.chainId;
            return Object(a.jsx)(a.Fragment, {
                children: b && h ? Object(a.jsx)(w.a, {
                    value: {
                        web3Provider: u,
                        ABIs: new Map([[g.d, m], [g.f, p]]),
                        refreshInterval: 8e3
                    },
                    children: Object(a.jsx)(D, {})
                }) : Object(a.jsxs)("div", {
                    className: "flex justify-center my-12",
                    children: [Object(a.jsx)(S.a, {
                        block: !1,
                        type: "primary",
                        disabled: c,
                        onClick: Object(x.a)(d.a.mark((function e() {
                            return d.a.wrap((function(e) {
                                for (; ; )
                                    switch (e.prev = e.next) {
                                    case 0:
                                        !o && l(!0);
                                    case 1:
                                    case "end":
                                        return e.stop()
                                    }
                            }
                            ), e)
                        }
                        ))),
                        children: r ? o ? "".concat(o.slice(0, 6), "...\n                ").concat(o.slice(o.length - 4, o.length)) : Object(a.jsx)("div", {
                            className: "flex items-center text-xl justify-center text-white",
                            children: Object(a.jsx)("span", {
                                children: "Connect Your Wallet"
                            })
                        }) : "Loading..."
                    }), Object(a.jsx)(A.a, {
                        visible: s,
                        onClose: function() {
                            return l(!1)
                        }
                    })]
                })
            })
        }
        function q() {
            var e = Object(n.useState)(!0)
              , t = (e[0],
            e[1])
              , s = Object(n.useState)("")
              , o = s[0]
              , d = s[1];
            return Object(n.useEffect)((function() {
                setTimeout((function() {
                    return t(!1)
                }
                ), 1500)
            }
            ), []),
            Object(a.jsxs)(r.a, {
                title: "Mint DMM",
                children: [Object(a.jsx)(l.a, {}), Object(a.jsxs)("div", {
                    className: "max-w-screen-xl mx-auto",
                    children: [Object(a.jsxs)("div", {
                        className: "text-white pt-20",
                        children: [Object(a.jsx)("div", {
                            className: "text-3xl flex justify-center",
                            children: "Degen Mad Meerkat Mint"
                        }), Object(a.jsxs)("div", {
                            className: "text-base flex justify-center py-4 text-pink-600 text-center",
                            children: ["Preparation Phase ", Object(a.jsx)("br", {}), "Genesis and CRO Minting will begin on Sunday 15th May 2022 1PM UTC."]
                        }), Object(a.jsx)("div", {
                            className: "text-base flex justify-center text-gray-500 text-center",
                            children: "Genesis Round is dedicated to those that hold Genesis Mad Meerkat."
                        }), Object(a.jsx)("div", {
                            className: "text-base flex justify-center text-gray-500 text-center",
                            children: "CRO Round is dedicated to those that hold Cro Mad Meerkat or Treehouse."
                        }), Object(a.jsx)("div", {
                            className: "text-base flex justify-center text-gray-500 text-center",
                            children: "Public Round is open for all."
                        }), Object(a.jsxs)("div", {
                            className: "text-xl flex justify-center text-center pt-4",
                            children: [Object(a.jsx)("button", {
                                className: "m-4 py-2 px-4 ".concat("GENESIS" === o ? "bg-gradient-to-r from-emerald-900 to-teal-700 text-gray-300 border-emerald-500 border-2" : "bg-gradient-to-r from-emerald-900 to-teal-700 hover:from-emerald-800 hover:to-teal-700", " rounded-xl"),
                                onClick: function() {
                                    d("GENESIS")
                                },
                                children: "Genesis"
                            }), Object(a.jsx)("button", {
                                className: "m-4 py-2 px-4 ".concat("CRO" === o ? "bg-gradient-to-r from-purple-900 to-pink-900 text-gray-300 border-purple-500 border-2" : "bg-gradient-to-r from-purple-800 to-pink-700 hover:from-purple-900 hover:to-pink-800", " rounded-xl"),
                                onClick: function() {
                                    d("CRO")
                                },
                                children: "CRO"
                            }), Object(a.jsx)("button", {
                                className: "m-4 py-2 px-4 ".concat("PUBLIC" === o ? "bg-gradient-to-r from-blue-900 to-sky-700 text-gray-300 border-blue-500 border-2" : "bg-gradient-to-r from-blue-800 to-sky-600 hover:from-blue-900 hover:to-sky-700", " rounded-xl"),
                                onClick: function() {
                                    d("PUBLIC")
                                },
                                children: "Public"
                            })]
                        })]
                    }), Object(a.jsxs)("div", {
                        className: "border-[#8652FF] z-20 pt-4",
                        children: ["GENESIS" === o && Object(a.jsx)(Q, {}), "CRO" === o && Object(a.jsx)(W, {}), "PUBLIC" === o && Object(a.jsx)(T, {})]
                    })]
                }), Object(a.jsx)(i.a, {}), Object(a.jsx)(c.a, {})]
            })
        }
    },
    xQut: function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return i
        }
        ));
        var a = s("cpVT")
          , n = s("nKUr")
          , l = s("xXRr");
        function c(e, t) {
            var s = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var a = Object.getOwnPropertySymbols(e);
                t && (a = a.filter((function(t) {
                    return Object.getOwnPropertyDescriptor(e, t).enumerable
                }
                ))),
                s.push.apply(s, a)
            }
            return s
        }
        function r(e) {
            for (var t = 1; t < arguments.length; t++) {
                var s = null != arguments[t] ? arguments[t] : {};
                t % 2 ? c(Object(s), !0).forEach((function(t) {
                    Object(a.a)(e, t, s[t])
                }
                )) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(s)) : c(Object(s)).forEach((function(t) {
                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(s, t))
                }
                ))
            }
            return e
        }
        function i(e) {
            var t = e.type
              , s = void 0 === t ? "primary" : t
              , a = e.className
              , c = void 0 === a ? "" : a
              , i = e.padded
              , o = void 0 === i || i
              , d = e.shadow
              , u = void 0 !== d && d
              , x = e.style
              , m = void 0 === x ? {} : x
              , p = function(e) {
                if ("secondary" === e)
                    return "bg-black bg-opacity-5 text-black dark:bg-gray-800 dark:text-white dark:bg-opacity-100 hover:bg-opacity-10 transition-opacity";
                if ("tertiary" === e)
                    return "bg-black bg-opacity-40 text-white hover:bg-opacity-50 transition-opacity";
                if ("danger" === e)
                    return "bg-red-500 text-white hover:bg-opacity-50 transition-opacity";
                return "bg-gradient-to-r from-purple-600 to-pink-600 hover:from-purple-700 hover:to-pink-700 text-white"
            }(s);
            return Object(n.jsx)("button", r(r({
                style: m,
                onClick: e.disabled ? void 0 : e.onClick
            }, e.htmlSubmit ? {
                type: "submit"
            } : {
                type: "button"
            }), {}, {
                className: "".concat(o ? "py-2 px-5" : "", " ").concat(null !== e && void 0 !== e && e.block ? "block" : "", " ").concat(null !== e && void 0 !== e && e.uppercase ? "uppercase" : "", " ").concat(e.disabled || e.loading ? "cursor-not-allowed" : "", " text-center group relative flex justify-center ").concat(p, " focus:outline-none cursor-pointer rounded-md ").concat(u ? "btn-shadow" : "", " ").concat(c),
                children: Object(n.jsxs)("span", {
                    className: "flex",
                    children: [Object(n.jsx)("span", {
                        className: "left-0 inset-y-0 flex items-center ".concat(e.icon || e.loading ? "pr-2" : ""),
                        children: e.loading ? Object(n.jsx)(l.a, {}) : e.icon
                    }), Object(n.jsx)("span", {
                        children: e.children
                    })]
                })
            }))
        }
    },
    xXRr: function(e, t, s) {
        "use strict";
        s.d(t, "a", (function() {
            return n
        }
        ));
        var a = s("nKUr");
        function n() {
            return Object(a.jsxs)("svg", {
                xmlns: "http://www.w3.org/2000/svg",
                viewBox: "0 0 24 24",
                "aria-hidden": "true",
                className: "animate-spin h-5 w-5 mr-3",
                children: [Object(a.jsx)("circle", {
                    className: "opacity-25",
                    cx: "12",
                    cy: "12",
                    r: "10",
                    stroke: "currentColor",
                    strokeWidth: "4"
                }), Object(a.jsx)("path", {
                    className: "opacity-75",
                    fill: "currentColor",
                    d: "M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                })]
            })
        }
    }
}, [["MwE2", 0, 1, 8, 9, 7, 2, 4, 3, 5, 6, 10, 11, 12, 13, 15, 16, 18]]]);
