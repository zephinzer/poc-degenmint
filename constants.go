package main

const (
	ContractAddressHexMadMeerkatDegenTest = "0x0e423e68dc5a6c706b1c657a8956878174e51701"
	ContractAddressHexMadMeerkatDegen     = "0xa19bfce9baf34b92923b71d487db9d0d051a88f8"
	ContractAddressToUse                  = ContractAddressHexMadMeerkatDegen
	GasLimitMultiplier                    = 3
	GasPriceMultiplier                    = 3
	MaxNumberPerTransaction               = 2
	PathToBytecode                        = "./data/bytecode"
	PathToKeys                            = "./secrets/keys.json"
	PaymentToken                          = "$MAD"
	PaymentTokenDecimalPlaces             = 1000000000000000000
	RpcAddress                            = "https://rpc.vvs.finance/"
	TestTrigger                           = true
)
