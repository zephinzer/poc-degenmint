// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// TokenMetaData contains all meta data concerning the Token contract.
var TokenMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"approved\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"AssetMinted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"MAD\",\"outputs\":[{\"internalType\":\"contractIERC20\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"MAX_DEGEN\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"MAX_DEGEN_PER_ROUND\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"MAX_DEGEN_PURCHASE\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"MMD_PROVENANCE\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"SNAPSHOT_PROXY_MMB\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"SNAPSHOT_PROXY_MMT\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"amountPurchased1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"amountPurchased2\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"amountPurchasedPublic\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"baseURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"degenPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"degenPricePresale1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"degenPricePresale2\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"flipPreSale1State\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"flipPreSale2State\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"flipSaleState\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numberOfTokens\",\"type\":\"uint256\"}],\"name\":\"giveAway\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_address\",\"type\":\"address\"}],\"name\":\"isWhitelist1\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_address\",\"type\":\"address\"}],\"name\":\"isWhitelist2\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_address\",\"type\":\"address\"}],\"name\":\"isWhitelist2Manual\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"numberOfTokens\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"mintDegen\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"numberOfTokens\",\"type\":\"uint256\"}],\"name\":\"mintLegendary\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"presale1Active\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"presale1reservedDegens\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"presale2Active\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"numberOfTokens\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"presalemintDegen1\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"numberOfTokens\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"presalemintDegen2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"reservedDegens\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"saleIsActive\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"baseURI\",\"type\":\"string\"}],\"name\":\"setBaseURI\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"setDegenPresalePrice1\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"setDegenPresalePrice2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_price\",\"type\":\"uint256\"}],\"name\":\"setDegenPrice\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"provenanceHash\",\"type\":\"string\"}],\"name\":\"setProvenanceHash\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_mmbProxy\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_mmtProxy\",\"type\":\"address\"}],\"name\":\"setSnapshotProxy\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"_account\",\"type\":\"address[]\"},{\"internalType\":\"bool[]\",\"name\":\"_activate\",\"type\":\"bool[]\"}],\"name\":\"setWhitelist1\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"_account\",\"type\":\"address[]\"},{\"internalType\":\"bool[]\",\"name\":\"_activate\",\"type\":\"bool[]\"}],\"name\":\"setWhitelist2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenOfOwnerByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"whitelist1\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"whitelist2\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"}],\"name\":\"withdrawERC20\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// TokenABI is the input ABI used to generate the binding from.
// Deprecated: Use TokenMetaData.ABI instead.
var TokenABI = TokenMetaData.ABI

// Token is an auto generated Go binding around an Ethereum contract.
type Token struct {
	TokenCaller     // Read-only binding to the contract
	TokenTransactor // Write-only binding to the contract
	TokenFilterer   // Log filterer for contract events
}

// TokenCaller is an auto generated read-only Go binding around an Ethereum contract.
type TokenCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TokenTransactor is an auto generated write-only Go binding around an Ethereum contract.
type TokenTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TokenFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type TokenFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TokenSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type TokenSession struct {
	Contract     *Token            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TokenCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type TokenCallerSession struct {
	Contract *TokenCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// TokenTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type TokenTransactorSession struct {
	Contract     *TokenTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TokenRaw is an auto generated low-level Go binding around an Ethereum contract.
type TokenRaw struct {
	Contract *Token // Generic contract binding to access the raw methods on
}

// TokenCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type TokenCallerRaw struct {
	Contract *TokenCaller // Generic read-only contract binding to access the raw methods on
}

// TokenTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type TokenTransactorRaw struct {
	Contract *TokenTransactor // Generic write-only contract binding to access the raw methods on
}

// NewToken creates a new instance of Token, bound to a specific deployed contract.
func NewToken(address common.Address, backend bind.ContractBackend) (*Token, error) {
	contract, err := bindToken(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Token{TokenCaller: TokenCaller{contract: contract}, TokenTransactor: TokenTransactor{contract: contract}, TokenFilterer: TokenFilterer{contract: contract}}, nil
}

// NewTokenCaller creates a new read-only instance of Token, bound to a specific deployed contract.
func NewTokenCaller(address common.Address, caller bind.ContractCaller) (*TokenCaller, error) {
	contract, err := bindToken(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &TokenCaller{contract: contract}, nil
}

// NewTokenTransactor creates a new write-only instance of Token, bound to a specific deployed contract.
func NewTokenTransactor(address common.Address, transactor bind.ContractTransactor) (*TokenTransactor, error) {
	contract, err := bindToken(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &TokenTransactor{contract: contract}, nil
}

// NewTokenFilterer creates a new log filterer instance of Token, bound to a specific deployed contract.
func NewTokenFilterer(address common.Address, filterer bind.ContractFilterer) (*TokenFilterer, error) {
	contract, err := bindToken(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &TokenFilterer{contract: contract}, nil
}

// bindToken binds a generic wrapper to an already deployed contract.
func bindToken(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(TokenABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Token *TokenRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Token.Contract.TokenCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Token *TokenRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.Contract.TokenTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Token *TokenRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Token.Contract.TokenTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Token *TokenCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Token.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Token *TokenTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Token *TokenTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Token.Contract.contract.Transact(opts, method, params...)
}

// MAD is a free data retrieval call binding the contract method 0x24292dd8.
//
// Solidity: function MAD() view returns(address)
func (_Token *TokenCaller) MAD(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "MAD")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// MAD is a free data retrieval call binding the contract method 0x24292dd8.
//
// Solidity: function MAD() view returns(address)
func (_Token *TokenSession) MAD() (common.Address, error) {
	return _Token.Contract.MAD(&_Token.CallOpts)
}

// MAD is a free data retrieval call binding the contract method 0x24292dd8.
//
// Solidity: function MAD() view returns(address)
func (_Token *TokenCallerSession) MAD() (common.Address, error) {
	return _Token.Contract.MAD(&_Token.CallOpts)
}

// MAXDEGEN is a free data retrieval call binding the contract method 0xf1e91920.
//
// Solidity: function MAX_DEGEN() view returns(uint256)
func (_Token *TokenCaller) MAXDEGEN(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "MAX_DEGEN")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MAXDEGEN is a free data retrieval call binding the contract method 0xf1e91920.
//
// Solidity: function MAX_DEGEN() view returns(uint256)
func (_Token *TokenSession) MAXDEGEN() (*big.Int, error) {
	return _Token.Contract.MAXDEGEN(&_Token.CallOpts)
}

// MAXDEGEN is a free data retrieval call binding the contract method 0xf1e91920.
//
// Solidity: function MAX_DEGEN() view returns(uint256)
func (_Token *TokenCallerSession) MAXDEGEN() (*big.Int, error) {
	return _Token.Contract.MAXDEGEN(&_Token.CallOpts)
}

// MAXDEGENPERROUND is a free data retrieval call binding the contract method 0xa60a1a12.
//
// Solidity: function MAX_DEGEN_PER_ROUND() view returns(uint256)
func (_Token *TokenCaller) MAXDEGENPERROUND(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "MAX_DEGEN_PER_ROUND")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MAXDEGENPERROUND is a free data retrieval call binding the contract method 0xa60a1a12.
//
// Solidity: function MAX_DEGEN_PER_ROUND() view returns(uint256)
func (_Token *TokenSession) MAXDEGENPERROUND() (*big.Int, error) {
	return _Token.Contract.MAXDEGENPERROUND(&_Token.CallOpts)
}

// MAXDEGENPERROUND is a free data retrieval call binding the contract method 0xa60a1a12.
//
// Solidity: function MAX_DEGEN_PER_ROUND() view returns(uint256)
func (_Token *TokenCallerSession) MAXDEGENPERROUND() (*big.Int, error) {
	return _Token.Contract.MAXDEGENPERROUND(&_Token.CallOpts)
}

// MAXDEGENPURCHASE is a free data retrieval call binding the contract method 0xef0ec237.
//
// Solidity: function MAX_DEGEN_PURCHASE() view returns(uint256)
func (_Token *TokenCaller) MAXDEGENPURCHASE(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "MAX_DEGEN_PURCHASE")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MAXDEGENPURCHASE is a free data retrieval call binding the contract method 0xef0ec237.
//
// Solidity: function MAX_DEGEN_PURCHASE() view returns(uint256)
func (_Token *TokenSession) MAXDEGENPURCHASE() (*big.Int, error) {
	return _Token.Contract.MAXDEGENPURCHASE(&_Token.CallOpts)
}

// MAXDEGENPURCHASE is a free data retrieval call binding the contract method 0xef0ec237.
//
// Solidity: function MAX_DEGEN_PURCHASE() view returns(uint256)
func (_Token *TokenCallerSession) MAXDEGENPURCHASE() (*big.Int, error) {
	return _Token.Contract.MAXDEGENPURCHASE(&_Token.CallOpts)
}

// MMDPROVENANCE is a free data retrieval call binding the contract method 0x31a34b74.
//
// Solidity: function MMD_PROVENANCE() view returns(string)
func (_Token *TokenCaller) MMDPROVENANCE(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "MMD_PROVENANCE")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// MMDPROVENANCE is a free data retrieval call binding the contract method 0x31a34b74.
//
// Solidity: function MMD_PROVENANCE() view returns(string)
func (_Token *TokenSession) MMDPROVENANCE() (string, error) {
	return _Token.Contract.MMDPROVENANCE(&_Token.CallOpts)
}

// MMDPROVENANCE is a free data retrieval call binding the contract method 0x31a34b74.
//
// Solidity: function MMD_PROVENANCE() view returns(string)
func (_Token *TokenCallerSession) MMDPROVENANCE() (string, error) {
	return _Token.Contract.MMDPROVENANCE(&_Token.CallOpts)
}

// SNAPSHOTPROXYMMB is a free data retrieval call binding the contract method 0x03f97f44.
//
// Solidity: function SNAPSHOT_PROXY_MMB() view returns(address)
func (_Token *TokenCaller) SNAPSHOTPROXYMMB(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "SNAPSHOT_PROXY_MMB")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// SNAPSHOTPROXYMMB is a free data retrieval call binding the contract method 0x03f97f44.
//
// Solidity: function SNAPSHOT_PROXY_MMB() view returns(address)
func (_Token *TokenSession) SNAPSHOTPROXYMMB() (common.Address, error) {
	return _Token.Contract.SNAPSHOTPROXYMMB(&_Token.CallOpts)
}

// SNAPSHOTPROXYMMB is a free data retrieval call binding the contract method 0x03f97f44.
//
// Solidity: function SNAPSHOT_PROXY_MMB() view returns(address)
func (_Token *TokenCallerSession) SNAPSHOTPROXYMMB() (common.Address, error) {
	return _Token.Contract.SNAPSHOTPROXYMMB(&_Token.CallOpts)
}

// SNAPSHOTPROXYMMT is a free data retrieval call binding the contract method 0xd1fd52cb.
//
// Solidity: function SNAPSHOT_PROXY_MMT() view returns(address)
func (_Token *TokenCaller) SNAPSHOTPROXYMMT(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "SNAPSHOT_PROXY_MMT")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// SNAPSHOTPROXYMMT is a free data retrieval call binding the contract method 0xd1fd52cb.
//
// Solidity: function SNAPSHOT_PROXY_MMT() view returns(address)
func (_Token *TokenSession) SNAPSHOTPROXYMMT() (common.Address, error) {
	return _Token.Contract.SNAPSHOTPROXYMMT(&_Token.CallOpts)
}

// SNAPSHOTPROXYMMT is a free data retrieval call binding the contract method 0xd1fd52cb.
//
// Solidity: function SNAPSHOT_PROXY_MMT() view returns(address)
func (_Token *TokenCallerSession) SNAPSHOTPROXYMMT() (common.Address, error) {
	return _Token.Contract.SNAPSHOTPROXYMMT(&_Token.CallOpts)
}

// AmountPurchased1 is a free data retrieval call binding the contract method 0xa1c01aa1.
//
// Solidity: function amountPurchased1(address ) view returns(uint256)
func (_Token *TokenCaller) AmountPurchased1(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "amountPurchased1", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AmountPurchased1 is a free data retrieval call binding the contract method 0xa1c01aa1.
//
// Solidity: function amountPurchased1(address ) view returns(uint256)
func (_Token *TokenSession) AmountPurchased1(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchased1(&_Token.CallOpts, arg0)
}

// AmountPurchased1 is a free data retrieval call binding the contract method 0xa1c01aa1.
//
// Solidity: function amountPurchased1(address ) view returns(uint256)
func (_Token *TokenCallerSession) AmountPurchased1(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchased1(&_Token.CallOpts, arg0)
}

// AmountPurchased2 is a free data retrieval call binding the contract method 0xce239da7.
//
// Solidity: function amountPurchased2(address ) view returns(uint256)
func (_Token *TokenCaller) AmountPurchased2(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "amountPurchased2", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AmountPurchased2 is a free data retrieval call binding the contract method 0xce239da7.
//
// Solidity: function amountPurchased2(address ) view returns(uint256)
func (_Token *TokenSession) AmountPurchased2(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchased2(&_Token.CallOpts, arg0)
}

// AmountPurchased2 is a free data retrieval call binding the contract method 0xce239da7.
//
// Solidity: function amountPurchased2(address ) view returns(uint256)
func (_Token *TokenCallerSession) AmountPurchased2(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchased2(&_Token.CallOpts, arg0)
}

// AmountPurchasedPublic is a free data retrieval call binding the contract method 0x25349555.
//
// Solidity: function amountPurchasedPublic(address ) view returns(uint256)
func (_Token *TokenCaller) AmountPurchasedPublic(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "amountPurchasedPublic", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AmountPurchasedPublic is a free data retrieval call binding the contract method 0x25349555.
//
// Solidity: function amountPurchasedPublic(address ) view returns(uint256)
func (_Token *TokenSession) AmountPurchasedPublic(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchasedPublic(&_Token.CallOpts, arg0)
}

// AmountPurchasedPublic is a free data retrieval call binding the contract method 0x25349555.
//
// Solidity: function amountPurchasedPublic(address ) view returns(uint256)
func (_Token *TokenCallerSession) AmountPurchasedPublic(arg0 common.Address) (*big.Int, error) {
	return _Token.Contract.AmountPurchasedPublic(&_Token.CallOpts, arg0)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Token *TokenCaller) BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Token *TokenSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _Token.Contract.BalanceOf(&_Token.CallOpts, owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_Token *TokenCallerSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _Token.Contract.BalanceOf(&_Token.CallOpts, owner)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Token *TokenCaller) BaseURI(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "baseURI")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Token *TokenSession) BaseURI() (string, error) {
	return _Token.Contract.BaseURI(&_Token.CallOpts)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_Token *TokenCallerSession) BaseURI() (string, error) {
	return _Token.Contract.BaseURI(&_Token.CallOpts)
}

// DegenPrice is a free data retrieval call binding the contract method 0x6dd47e40.
//
// Solidity: function degenPrice() view returns(uint256)
func (_Token *TokenCaller) DegenPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "degenPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DegenPrice is a free data retrieval call binding the contract method 0x6dd47e40.
//
// Solidity: function degenPrice() view returns(uint256)
func (_Token *TokenSession) DegenPrice() (*big.Int, error) {
	return _Token.Contract.DegenPrice(&_Token.CallOpts)
}

// DegenPrice is a free data retrieval call binding the contract method 0x6dd47e40.
//
// Solidity: function degenPrice() view returns(uint256)
func (_Token *TokenCallerSession) DegenPrice() (*big.Int, error) {
	return _Token.Contract.DegenPrice(&_Token.CallOpts)
}

// DegenPricePresale1 is a free data retrieval call binding the contract method 0x3e7cc51b.
//
// Solidity: function degenPricePresale1() view returns(uint256)
func (_Token *TokenCaller) DegenPricePresale1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "degenPricePresale1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DegenPricePresale1 is a free data retrieval call binding the contract method 0x3e7cc51b.
//
// Solidity: function degenPricePresale1() view returns(uint256)
func (_Token *TokenSession) DegenPricePresale1() (*big.Int, error) {
	return _Token.Contract.DegenPricePresale1(&_Token.CallOpts)
}

// DegenPricePresale1 is a free data retrieval call binding the contract method 0x3e7cc51b.
//
// Solidity: function degenPricePresale1() view returns(uint256)
func (_Token *TokenCallerSession) DegenPricePresale1() (*big.Int, error) {
	return _Token.Contract.DegenPricePresale1(&_Token.CallOpts)
}

// DegenPricePresale2 is a free data retrieval call binding the contract method 0xb2d6a454.
//
// Solidity: function degenPricePresale2() view returns(uint256)
func (_Token *TokenCaller) DegenPricePresale2(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "degenPricePresale2")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// DegenPricePresale2 is a free data retrieval call binding the contract method 0xb2d6a454.
//
// Solidity: function degenPricePresale2() view returns(uint256)
func (_Token *TokenSession) DegenPricePresale2() (*big.Int, error) {
	return _Token.Contract.DegenPricePresale2(&_Token.CallOpts)
}

// DegenPricePresale2 is a free data retrieval call binding the contract method 0xb2d6a454.
//
// Solidity: function degenPricePresale2() view returns(uint256)
func (_Token *TokenCallerSession) DegenPricePresale2() (*big.Int, error) {
	return _Token.Contract.DegenPricePresale2(&_Token.CallOpts)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Token *TokenCaller) GetApproved(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "getApproved", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Token *TokenSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _Token.Contract.GetApproved(&_Token.CallOpts, tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_Token *TokenCallerSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _Token.Contract.GetApproved(&_Token.CallOpts, tokenId)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Token *TokenCaller) IsApprovedForAll(opts *bind.CallOpts, owner common.Address, operator common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "isApprovedForAll", owner, operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Token *TokenSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _Token.Contract.IsApprovedForAll(&_Token.CallOpts, owner, operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_Token *TokenCallerSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _Token.Contract.IsApprovedForAll(&_Token.CallOpts, owner, operator)
}

// IsWhitelist1 is a free data retrieval call binding the contract method 0x3f5aef12.
//
// Solidity: function isWhitelist1(address _address) view returns(bool)
func (_Token *TokenCaller) IsWhitelist1(opts *bind.CallOpts, _address common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "isWhitelist1", _address)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelist1 is a free data retrieval call binding the contract method 0x3f5aef12.
//
// Solidity: function isWhitelist1(address _address) view returns(bool)
func (_Token *TokenSession) IsWhitelist1(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist1(&_Token.CallOpts, _address)
}

// IsWhitelist1 is a free data retrieval call binding the contract method 0x3f5aef12.
//
// Solidity: function isWhitelist1(address _address) view returns(bool)
func (_Token *TokenCallerSession) IsWhitelist1(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist1(&_Token.CallOpts, _address)
}

// IsWhitelist2 is a free data retrieval call binding the contract method 0x56070e79.
//
// Solidity: function isWhitelist2(address _address) view returns(bool)
func (_Token *TokenCaller) IsWhitelist2(opts *bind.CallOpts, _address common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "isWhitelist2", _address)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelist2 is a free data retrieval call binding the contract method 0x56070e79.
//
// Solidity: function isWhitelist2(address _address) view returns(bool)
func (_Token *TokenSession) IsWhitelist2(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist2(&_Token.CallOpts, _address)
}

// IsWhitelist2 is a free data retrieval call binding the contract method 0x56070e79.
//
// Solidity: function isWhitelist2(address _address) view returns(bool)
func (_Token *TokenCallerSession) IsWhitelist2(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist2(&_Token.CallOpts, _address)
}

// IsWhitelist2Manual is a free data retrieval call binding the contract method 0x3ae98c5e.
//
// Solidity: function isWhitelist2Manual(address _address) view returns(bool)
func (_Token *TokenCaller) IsWhitelist2Manual(opts *bind.CallOpts, _address common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "isWhitelist2Manual", _address)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsWhitelist2Manual is a free data retrieval call binding the contract method 0x3ae98c5e.
//
// Solidity: function isWhitelist2Manual(address _address) view returns(bool)
func (_Token *TokenSession) IsWhitelist2Manual(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist2Manual(&_Token.CallOpts, _address)
}

// IsWhitelist2Manual is a free data retrieval call binding the contract method 0x3ae98c5e.
//
// Solidity: function isWhitelist2Manual(address _address) view returns(bool)
func (_Token *TokenCallerSession) IsWhitelist2Manual(_address common.Address) (bool, error) {
	return _Token.Contract.IsWhitelist2Manual(&_Token.CallOpts, _address)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Token *TokenCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Token *TokenSession) Name() (string, error) {
	return _Token.Contract.Name(&_Token.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_Token *TokenCallerSession) Name() (string, error) {
	return _Token.Contract.Name(&_Token.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Token *TokenCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Token *TokenSession) Owner() (common.Address, error) {
	return _Token.Contract.Owner(&_Token.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Token *TokenCallerSession) Owner() (common.Address, error) {
	return _Token.Contract.Owner(&_Token.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Token *TokenCaller) OwnerOf(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "ownerOf", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Token *TokenSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _Token.Contract.OwnerOf(&_Token.CallOpts, tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_Token *TokenCallerSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _Token.Contract.OwnerOf(&_Token.CallOpts, tokenId)
}

// Presale1Active is a free data retrieval call binding the contract method 0x37419a81.
//
// Solidity: function presale1Active() view returns(bool)
func (_Token *TokenCaller) Presale1Active(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "presale1Active")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Presale1Active is a free data retrieval call binding the contract method 0x37419a81.
//
// Solidity: function presale1Active() view returns(bool)
func (_Token *TokenSession) Presale1Active() (bool, error) {
	return _Token.Contract.Presale1Active(&_Token.CallOpts)
}

// Presale1Active is a free data retrieval call binding the contract method 0x37419a81.
//
// Solidity: function presale1Active() view returns(bool)
func (_Token *TokenCallerSession) Presale1Active() (bool, error) {
	return _Token.Contract.Presale1Active(&_Token.CallOpts)
}

// Presale1reservedDegens is a free data retrieval call binding the contract method 0x98ef6536.
//
// Solidity: function presale1reservedDegens() view returns(uint256)
func (_Token *TokenCaller) Presale1reservedDegens(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "presale1reservedDegens")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Presale1reservedDegens is a free data retrieval call binding the contract method 0x98ef6536.
//
// Solidity: function presale1reservedDegens() view returns(uint256)
func (_Token *TokenSession) Presale1reservedDegens() (*big.Int, error) {
	return _Token.Contract.Presale1reservedDegens(&_Token.CallOpts)
}

// Presale1reservedDegens is a free data retrieval call binding the contract method 0x98ef6536.
//
// Solidity: function presale1reservedDegens() view returns(uint256)
func (_Token *TokenCallerSession) Presale1reservedDegens() (*big.Int, error) {
	return _Token.Contract.Presale1reservedDegens(&_Token.CallOpts)
}

// Presale2Active is a free data retrieval call binding the contract method 0x0256edeb.
//
// Solidity: function presale2Active() view returns(bool)
func (_Token *TokenCaller) Presale2Active(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "presale2Active")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Presale2Active is a free data retrieval call binding the contract method 0x0256edeb.
//
// Solidity: function presale2Active() view returns(bool)
func (_Token *TokenSession) Presale2Active() (bool, error) {
	return _Token.Contract.Presale2Active(&_Token.CallOpts)
}

// Presale2Active is a free data retrieval call binding the contract method 0x0256edeb.
//
// Solidity: function presale2Active() view returns(bool)
func (_Token *TokenCallerSession) Presale2Active() (bool, error) {
	return _Token.Contract.Presale2Active(&_Token.CallOpts)
}

// ReservedDegens is a free data retrieval call binding the contract method 0xe4c2144d.
//
// Solidity: function reservedDegens() view returns(uint256)
func (_Token *TokenCaller) ReservedDegens(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "reservedDegens")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ReservedDegens is a free data retrieval call binding the contract method 0xe4c2144d.
//
// Solidity: function reservedDegens() view returns(uint256)
func (_Token *TokenSession) ReservedDegens() (*big.Int, error) {
	return _Token.Contract.ReservedDegens(&_Token.CallOpts)
}

// ReservedDegens is a free data retrieval call binding the contract method 0xe4c2144d.
//
// Solidity: function reservedDegens() view returns(uint256)
func (_Token *TokenCallerSession) ReservedDegens() (*big.Int, error) {
	return _Token.Contract.ReservedDegens(&_Token.CallOpts)
}

// SaleIsActive is a free data retrieval call binding the contract method 0xeb8d2444.
//
// Solidity: function saleIsActive() view returns(bool)
func (_Token *TokenCaller) SaleIsActive(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "saleIsActive")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SaleIsActive is a free data retrieval call binding the contract method 0xeb8d2444.
//
// Solidity: function saleIsActive() view returns(bool)
func (_Token *TokenSession) SaleIsActive() (bool, error) {
	return _Token.Contract.SaleIsActive(&_Token.CallOpts)
}

// SaleIsActive is a free data retrieval call binding the contract method 0xeb8d2444.
//
// Solidity: function saleIsActive() view returns(bool)
func (_Token *TokenCallerSession) SaleIsActive() (bool, error) {
	return _Token.Contract.SaleIsActive(&_Token.CallOpts)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Token *TokenCaller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Token *TokenSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Token.Contract.SupportsInterface(&_Token.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_Token *TokenCallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _Token.Contract.SupportsInterface(&_Token.CallOpts, interfaceId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Token *TokenCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Token *TokenSession) Symbol() (string, error) {
	return _Token.Contract.Symbol(&_Token.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_Token *TokenCallerSession) Symbol() (string, error) {
	return _Token.Contract.Symbol(&_Token.CallOpts)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Token *TokenCaller) TokenByIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "tokenByIndex", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Token *TokenSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Token.Contract.TokenByIndex(&_Token.CallOpts, index)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_Token *TokenCallerSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _Token.Contract.TokenByIndex(&_Token.CallOpts, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Token *TokenCaller) TokenOfOwnerByIndex(opts *bind.CallOpts, owner common.Address, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "tokenOfOwnerByIndex", owner, index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Token *TokenSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _Token.Contract.TokenOfOwnerByIndex(&_Token.CallOpts, owner, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_Token *TokenCallerSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _Token.Contract.TokenOfOwnerByIndex(&_Token.CallOpts, owner, index)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Token *TokenCaller) TokenURI(opts *bind.CallOpts, tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "tokenURI", tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Token *TokenSession) TokenURI(tokenId *big.Int) (string, error) {
	return _Token.Contract.TokenURI(&_Token.CallOpts, tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_Token *TokenCallerSession) TokenURI(tokenId *big.Int) (string, error) {
	return _Token.Contract.TokenURI(&_Token.CallOpts, tokenId)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Token *TokenCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Token *TokenSession) TotalSupply() (*big.Int, error) {
	return _Token.Contract.TotalSupply(&_Token.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_Token *TokenCallerSession) TotalSupply() (*big.Int, error) {
	return _Token.Contract.TotalSupply(&_Token.CallOpts)
}

// Whitelist1 is a free data retrieval call binding the contract method 0xfcdf3ae2.
//
// Solidity: function whitelist1(address ) view returns(bool)
func (_Token *TokenCaller) Whitelist1(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "whitelist1", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Whitelist1 is a free data retrieval call binding the contract method 0xfcdf3ae2.
//
// Solidity: function whitelist1(address ) view returns(bool)
func (_Token *TokenSession) Whitelist1(arg0 common.Address) (bool, error) {
	return _Token.Contract.Whitelist1(&_Token.CallOpts, arg0)
}

// Whitelist1 is a free data retrieval call binding the contract method 0xfcdf3ae2.
//
// Solidity: function whitelist1(address ) view returns(bool)
func (_Token *TokenCallerSession) Whitelist1(arg0 common.Address) (bool, error) {
	return _Token.Contract.Whitelist1(&_Token.CallOpts, arg0)
}

// Whitelist2 is a free data retrieval call binding the contract method 0x3ad504c0.
//
// Solidity: function whitelist2(address ) view returns(bool)
func (_Token *TokenCaller) Whitelist2(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _Token.contract.Call(opts, &out, "whitelist2", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Whitelist2 is a free data retrieval call binding the contract method 0x3ad504c0.
//
// Solidity: function whitelist2(address ) view returns(bool)
func (_Token *TokenSession) Whitelist2(arg0 common.Address) (bool, error) {
	return _Token.Contract.Whitelist2(&_Token.CallOpts, arg0)
}

// Whitelist2 is a free data retrieval call binding the contract method 0x3ad504c0.
//
// Solidity: function whitelist2(address ) view returns(bool)
func (_Token *TokenCallerSession) Whitelist2(arg0 common.Address) (bool, error) {
	return _Token.Contract.Whitelist2(&_Token.CallOpts, arg0)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Token *TokenTransactor) Approve(opts *bind.TransactOpts, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "approve", to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Token *TokenSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.Approve(&_Token.TransactOpts, to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_Token *TokenTransactorSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.Approve(&_Token.TransactOpts, to, tokenId)
}

// FlipPreSale1State is a paid mutator transaction binding the contract method 0xeaae09e0.
//
// Solidity: function flipPreSale1State() returns()
func (_Token *TokenTransactor) FlipPreSale1State(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "flipPreSale1State")
}

// FlipPreSale1State is a paid mutator transaction binding the contract method 0xeaae09e0.
//
// Solidity: function flipPreSale1State() returns()
func (_Token *TokenSession) FlipPreSale1State() (*types.Transaction, error) {
	return _Token.Contract.FlipPreSale1State(&_Token.TransactOpts)
}

// FlipPreSale1State is a paid mutator transaction binding the contract method 0xeaae09e0.
//
// Solidity: function flipPreSale1State() returns()
func (_Token *TokenTransactorSession) FlipPreSale1State() (*types.Transaction, error) {
	return _Token.Contract.FlipPreSale1State(&_Token.TransactOpts)
}

// FlipPreSale2State is a paid mutator transaction binding the contract method 0x8b2fe001.
//
// Solidity: function flipPreSale2State() returns()
func (_Token *TokenTransactor) FlipPreSale2State(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "flipPreSale2State")
}

// FlipPreSale2State is a paid mutator transaction binding the contract method 0x8b2fe001.
//
// Solidity: function flipPreSale2State() returns()
func (_Token *TokenSession) FlipPreSale2State() (*types.Transaction, error) {
	return _Token.Contract.FlipPreSale2State(&_Token.TransactOpts)
}

// FlipPreSale2State is a paid mutator transaction binding the contract method 0x8b2fe001.
//
// Solidity: function flipPreSale2State() returns()
func (_Token *TokenTransactorSession) FlipPreSale2State() (*types.Transaction, error) {
	return _Token.Contract.FlipPreSale2State(&_Token.TransactOpts)
}

// FlipSaleState is a paid mutator transaction binding the contract method 0x34918dfd.
//
// Solidity: function flipSaleState() returns()
func (_Token *TokenTransactor) FlipSaleState(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "flipSaleState")
}

// FlipSaleState is a paid mutator transaction binding the contract method 0x34918dfd.
//
// Solidity: function flipSaleState() returns()
func (_Token *TokenSession) FlipSaleState() (*types.Transaction, error) {
	return _Token.Contract.FlipSaleState(&_Token.TransactOpts)
}

// FlipSaleState is a paid mutator transaction binding the contract method 0x34918dfd.
//
// Solidity: function flipSaleState() returns()
func (_Token *TokenTransactorSession) FlipSaleState() (*types.Transaction, error) {
	return _Token.Contract.FlipSaleState(&_Token.TransactOpts)
}

// GiveAway is a paid mutator transaction binding the contract method 0xca800144.
//
// Solidity: function giveAway(address to, uint256 numberOfTokens) returns()
func (_Token *TokenTransactor) GiveAway(opts *bind.TransactOpts, to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "giveAway", to, numberOfTokens)
}

// GiveAway is a paid mutator transaction binding the contract method 0xca800144.
//
// Solidity: function giveAway(address to, uint256 numberOfTokens) returns()
func (_Token *TokenSession) GiveAway(to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.Contract.GiveAway(&_Token.TransactOpts, to, numberOfTokens)
}

// GiveAway is a paid mutator transaction binding the contract method 0xca800144.
//
// Solidity: function giveAway(address to, uint256 numberOfTokens) returns()
func (_Token *TokenTransactorSession) GiveAway(to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.Contract.GiveAway(&_Token.TransactOpts, to, numberOfTokens)
}

// MintDegen is a paid mutator transaction binding the contract method 0x077f103b.
//
// Solidity: function mintDegen(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactor) MintDegen(opts *bind.TransactOpts, numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "mintDegen", numberOfTokens, amount)
}

// MintDegen is a paid mutator transaction binding the contract method 0x077f103b.
//
// Solidity: function mintDegen(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenSession) MintDegen(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.MintDegen(&_Token.TransactOpts, numberOfTokens, amount)
}

// MintDegen is a paid mutator transaction binding the contract method 0x077f103b.
//
// Solidity: function mintDegen(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactorSession) MintDegen(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.MintDegen(&_Token.TransactOpts, numberOfTokens, amount)
}

// MintLegendary is a paid mutator transaction binding the contract method 0x86698e8b.
//
// Solidity: function mintLegendary(address to, uint256 numberOfTokens) returns()
func (_Token *TokenTransactor) MintLegendary(opts *bind.TransactOpts, to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "mintLegendary", to, numberOfTokens)
}

// MintLegendary is a paid mutator transaction binding the contract method 0x86698e8b.
//
// Solidity: function mintLegendary(address to, uint256 numberOfTokens) returns()
func (_Token *TokenSession) MintLegendary(to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.Contract.MintLegendary(&_Token.TransactOpts, to, numberOfTokens)
}

// MintLegendary is a paid mutator transaction binding the contract method 0x86698e8b.
//
// Solidity: function mintLegendary(address to, uint256 numberOfTokens) returns()
func (_Token *TokenTransactorSession) MintLegendary(to common.Address, numberOfTokens *big.Int) (*types.Transaction, error) {
	return _Token.Contract.MintLegendary(&_Token.TransactOpts, to, numberOfTokens)
}

// PresalemintDegen1 is a paid mutator transaction binding the contract method 0xd18e54bb.
//
// Solidity: function presalemintDegen1(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactor) PresalemintDegen1(opts *bind.TransactOpts, numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "presalemintDegen1", numberOfTokens, amount)
}

// PresalemintDegen1 is a paid mutator transaction binding the contract method 0xd18e54bb.
//
// Solidity: function presalemintDegen1(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenSession) PresalemintDegen1(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.PresalemintDegen1(&_Token.TransactOpts, numberOfTokens, amount)
}

// PresalemintDegen1 is a paid mutator transaction binding the contract method 0xd18e54bb.
//
// Solidity: function presalemintDegen1(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactorSession) PresalemintDegen1(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.PresalemintDegen1(&_Token.TransactOpts, numberOfTokens, amount)
}

// PresalemintDegen2 is a paid mutator transaction binding the contract method 0xcfb3bdfd.
//
// Solidity: function presalemintDegen2(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactor) PresalemintDegen2(opts *bind.TransactOpts, numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "presalemintDegen2", numberOfTokens, amount)
}

// PresalemintDegen2 is a paid mutator transaction binding the contract method 0xcfb3bdfd.
//
// Solidity: function presalemintDegen2(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenSession) PresalemintDegen2(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.PresalemintDegen2(&_Token.TransactOpts, numberOfTokens, amount)
}

// PresalemintDegen2 is a paid mutator transaction binding the contract method 0xcfb3bdfd.
//
// Solidity: function presalemintDegen2(uint256 numberOfTokens, uint256 amount) returns()
func (_Token *TokenTransactorSession) PresalemintDegen2(numberOfTokens *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Token.Contract.PresalemintDegen2(&_Token.TransactOpts, numberOfTokens, amount)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Token *TokenTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Token *TokenSession) RenounceOwnership() (*types.Transaction, error) {
	return _Token.Contract.RenounceOwnership(&_Token.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Token *TokenTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Token.Contract.RenounceOwnership(&_Token.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenTransactor) SafeTransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "safeTransferFrom", from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SafeTransferFrom(&_Token.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenTransactorSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SafeTransferFrom(&_Token.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Token *TokenTransactor) SafeTransferFrom0(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "safeTransferFrom0", from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Token *TokenSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Token.Contract.SafeTransferFrom0(&_Token.TransactOpts, from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_Token *TokenTransactorSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _Token.Contract.SafeTransferFrom0(&_Token.TransactOpts, from, to, tokenId, _data)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Token *TokenTransactor) SetApprovalForAll(opts *bind.TransactOpts, operator common.Address, approved bool) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setApprovalForAll", operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Token *TokenSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _Token.Contract.SetApprovalForAll(&_Token.TransactOpts, operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_Token *TokenTransactorSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _Token.Contract.SetApprovalForAll(&_Token.TransactOpts, operator, approved)
}

// SetBaseURI is a paid mutator transaction binding the contract method 0x55f804b3.
//
// Solidity: function setBaseURI(string baseURI) returns()
func (_Token *TokenTransactor) SetBaseURI(opts *bind.TransactOpts, baseURI string) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setBaseURI", baseURI)
}

// SetBaseURI is a paid mutator transaction binding the contract method 0x55f804b3.
//
// Solidity: function setBaseURI(string baseURI) returns()
func (_Token *TokenSession) SetBaseURI(baseURI string) (*types.Transaction, error) {
	return _Token.Contract.SetBaseURI(&_Token.TransactOpts, baseURI)
}

// SetBaseURI is a paid mutator transaction binding the contract method 0x55f804b3.
//
// Solidity: function setBaseURI(string baseURI) returns()
func (_Token *TokenTransactorSession) SetBaseURI(baseURI string) (*types.Transaction, error) {
	return _Token.Contract.SetBaseURI(&_Token.TransactOpts, baseURI)
}

// SetDegenPresalePrice1 is a paid mutator transaction binding the contract method 0x88874b73.
//
// Solidity: function setDegenPresalePrice1(uint256 _price) returns()
func (_Token *TokenTransactor) SetDegenPresalePrice1(opts *bind.TransactOpts, _price *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setDegenPresalePrice1", _price)
}

// SetDegenPresalePrice1 is a paid mutator transaction binding the contract method 0x88874b73.
//
// Solidity: function setDegenPresalePrice1(uint256 _price) returns()
func (_Token *TokenSession) SetDegenPresalePrice1(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPresalePrice1(&_Token.TransactOpts, _price)
}

// SetDegenPresalePrice1 is a paid mutator transaction binding the contract method 0x88874b73.
//
// Solidity: function setDegenPresalePrice1(uint256 _price) returns()
func (_Token *TokenTransactorSession) SetDegenPresalePrice1(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPresalePrice1(&_Token.TransactOpts, _price)
}

// SetDegenPresalePrice2 is a paid mutator transaction binding the contract method 0x91cf0bd5.
//
// Solidity: function setDegenPresalePrice2(uint256 _price) returns()
func (_Token *TokenTransactor) SetDegenPresalePrice2(opts *bind.TransactOpts, _price *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setDegenPresalePrice2", _price)
}

// SetDegenPresalePrice2 is a paid mutator transaction binding the contract method 0x91cf0bd5.
//
// Solidity: function setDegenPresalePrice2(uint256 _price) returns()
func (_Token *TokenSession) SetDegenPresalePrice2(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPresalePrice2(&_Token.TransactOpts, _price)
}

// SetDegenPresalePrice2 is a paid mutator transaction binding the contract method 0x91cf0bd5.
//
// Solidity: function setDegenPresalePrice2(uint256 _price) returns()
func (_Token *TokenTransactorSession) SetDegenPresalePrice2(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPresalePrice2(&_Token.TransactOpts, _price)
}

// SetDegenPrice is a paid mutator transaction binding the contract method 0xccd56191.
//
// Solidity: function setDegenPrice(uint256 _price) returns()
func (_Token *TokenTransactor) SetDegenPrice(opts *bind.TransactOpts, _price *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setDegenPrice", _price)
}

// SetDegenPrice is a paid mutator transaction binding the contract method 0xccd56191.
//
// Solidity: function setDegenPrice(uint256 _price) returns()
func (_Token *TokenSession) SetDegenPrice(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPrice(&_Token.TransactOpts, _price)
}

// SetDegenPrice is a paid mutator transaction binding the contract method 0xccd56191.
//
// Solidity: function setDegenPrice(uint256 _price) returns()
func (_Token *TokenTransactorSession) SetDegenPrice(_price *big.Int) (*types.Transaction, error) {
	return _Token.Contract.SetDegenPrice(&_Token.TransactOpts, _price)
}

// SetProvenanceHash is a paid mutator transaction binding the contract method 0x10969523.
//
// Solidity: function setProvenanceHash(string provenanceHash) returns()
func (_Token *TokenTransactor) SetProvenanceHash(opts *bind.TransactOpts, provenanceHash string) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setProvenanceHash", provenanceHash)
}

// SetProvenanceHash is a paid mutator transaction binding the contract method 0x10969523.
//
// Solidity: function setProvenanceHash(string provenanceHash) returns()
func (_Token *TokenSession) SetProvenanceHash(provenanceHash string) (*types.Transaction, error) {
	return _Token.Contract.SetProvenanceHash(&_Token.TransactOpts, provenanceHash)
}

// SetProvenanceHash is a paid mutator transaction binding the contract method 0x10969523.
//
// Solidity: function setProvenanceHash(string provenanceHash) returns()
func (_Token *TokenTransactorSession) SetProvenanceHash(provenanceHash string) (*types.Transaction, error) {
	return _Token.Contract.SetProvenanceHash(&_Token.TransactOpts, provenanceHash)
}

// SetSnapshotProxy is a paid mutator transaction binding the contract method 0x2711c94e.
//
// Solidity: function setSnapshotProxy(address _mmbProxy, address _mmtProxy) returns()
func (_Token *TokenTransactor) SetSnapshotProxy(opts *bind.TransactOpts, _mmbProxy common.Address, _mmtProxy common.Address) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setSnapshotProxy", _mmbProxy, _mmtProxy)
}

// SetSnapshotProxy is a paid mutator transaction binding the contract method 0x2711c94e.
//
// Solidity: function setSnapshotProxy(address _mmbProxy, address _mmtProxy) returns()
func (_Token *TokenSession) SetSnapshotProxy(_mmbProxy common.Address, _mmtProxy common.Address) (*types.Transaction, error) {
	return _Token.Contract.SetSnapshotProxy(&_Token.TransactOpts, _mmbProxy, _mmtProxy)
}

// SetSnapshotProxy is a paid mutator transaction binding the contract method 0x2711c94e.
//
// Solidity: function setSnapshotProxy(address _mmbProxy, address _mmtProxy) returns()
func (_Token *TokenTransactorSession) SetSnapshotProxy(_mmbProxy common.Address, _mmtProxy common.Address) (*types.Transaction, error) {
	return _Token.Contract.SetSnapshotProxy(&_Token.TransactOpts, _mmbProxy, _mmtProxy)
}

// SetWhitelist1 is a paid mutator transaction binding the contract method 0x907c10fb.
//
// Solidity: function setWhitelist1(address[] _account, bool[] _activate) returns()
func (_Token *TokenTransactor) SetWhitelist1(opts *bind.TransactOpts, _account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setWhitelist1", _account, _activate)
}

// SetWhitelist1 is a paid mutator transaction binding the contract method 0x907c10fb.
//
// Solidity: function setWhitelist1(address[] _account, bool[] _activate) returns()
func (_Token *TokenSession) SetWhitelist1(_account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.Contract.SetWhitelist1(&_Token.TransactOpts, _account, _activate)
}

// SetWhitelist1 is a paid mutator transaction binding the contract method 0x907c10fb.
//
// Solidity: function setWhitelist1(address[] _account, bool[] _activate) returns()
func (_Token *TokenTransactorSession) SetWhitelist1(_account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.Contract.SetWhitelist1(&_Token.TransactOpts, _account, _activate)
}

// SetWhitelist2 is a paid mutator transaction binding the contract method 0x16ea3863.
//
// Solidity: function setWhitelist2(address[] _account, bool[] _activate) returns()
func (_Token *TokenTransactor) SetWhitelist2(opts *bind.TransactOpts, _account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "setWhitelist2", _account, _activate)
}

// SetWhitelist2 is a paid mutator transaction binding the contract method 0x16ea3863.
//
// Solidity: function setWhitelist2(address[] _account, bool[] _activate) returns()
func (_Token *TokenSession) SetWhitelist2(_account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.Contract.SetWhitelist2(&_Token.TransactOpts, _account, _activate)
}

// SetWhitelist2 is a paid mutator transaction binding the contract method 0x16ea3863.
//
// Solidity: function setWhitelist2(address[] _account, bool[] _activate) returns()
func (_Token *TokenTransactorSession) SetWhitelist2(_account []common.Address, _activate []bool) (*types.Transaction, error) {
	return _Token.Contract.SetWhitelist2(&_Token.TransactOpts, _account, _activate)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenTransactor) TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "transferFrom", from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.TransferFrom(&_Token.TransactOpts, from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_Token *TokenTransactorSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _Token.Contract.TransferFrom(&_Token.TransactOpts, from, to, tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Token *TokenTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Token *TokenSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Token.Contract.TransferOwnership(&_Token.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Token *TokenTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Token.Contract.TransferOwnership(&_Token.TransactOpts, newOwner)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Token *TokenTransactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Token *TokenSession) Withdraw() (*types.Transaction, error) {
	return _Token.Contract.Withdraw(&_Token.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_Token *TokenTransactorSession) Withdraw() (*types.Transaction, error) {
	return _Token.Contract.Withdraw(&_Token.TransactOpts)
}

// WithdrawERC20 is a paid mutator transaction binding the contract method 0xf4f3b200.
//
// Solidity: function withdrawERC20(address token) returns()
func (_Token *TokenTransactor) WithdrawERC20(opts *bind.TransactOpts, token common.Address) (*types.Transaction, error) {
	return _Token.contract.Transact(opts, "withdrawERC20", token)
}

// WithdrawERC20 is a paid mutator transaction binding the contract method 0xf4f3b200.
//
// Solidity: function withdrawERC20(address token) returns()
func (_Token *TokenSession) WithdrawERC20(token common.Address) (*types.Transaction, error) {
	return _Token.Contract.WithdrawERC20(&_Token.TransactOpts, token)
}

// WithdrawERC20 is a paid mutator transaction binding the contract method 0xf4f3b200.
//
// Solidity: function withdrawERC20(address token) returns()
func (_Token *TokenTransactorSession) WithdrawERC20(token common.Address) (*types.Transaction, error) {
	return _Token.Contract.WithdrawERC20(&_Token.TransactOpts, token)
}

// TokenApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Token contract.
type TokenApprovalIterator struct {
	Event *TokenApproval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TokenApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TokenApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TokenApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TokenApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TokenApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TokenApproval represents a Approval event raised by the Token contract.
type TokenApproval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Token *TokenFilterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, tokenId []*big.Int) (*TokenApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Token.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &TokenApprovalIterator{contract: _Token.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Token *TokenFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *TokenApproval, owner []common.Address, approved []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Token.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TokenApproval)
				if err := _Token.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_Token *TokenFilterer) ParseApproval(log types.Log) (*TokenApproval, error) {
	event := new(TokenApproval)
	if err := _Token.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TokenApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the Token contract.
type TokenApprovalForAllIterator struct {
	Event *TokenApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TokenApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TokenApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TokenApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TokenApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TokenApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TokenApprovalForAll represents a ApprovalForAll event raised by the Token contract.
type TokenApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Token *TokenFilterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*TokenApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Token.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &TokenApprovalForAllIterator{contract: _Token.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Token *TokenFilterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *TokenApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _Token.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TokenApprovalForAll)
				if err := _Token.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_Token *TokenFilterer) ParseApprovalForAll(log types.Log) (*TokenApprovalForAll, error) {
	event := new(TokenApprovalForAll)
	if err := _Token.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TokenAssetMintedIterator is returned from FilterAssetMinted and is used to iterate over the raw logs and unpacked data for AssetMinted events raised by the Token contract.
type TokenAssetMintedIterator struct {
	Event *TokenAssetMinted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TokenAssetMintedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TokenAssetMinted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TokenAssetMinted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TokenAssetMintedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TokenAssetMintedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TokenAssetMinted represents a AssetMinted event raised by the Token contract.
type TokenAssetMinted struct {
	Account common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterAssetMinted is a free log retrieval operation binding the contract event 0x8d1457c1d60a6987eabbac898a50d8d7c81ba604c563663d2807027a4b079054.
//
// Solidity: event AssetMinted(address account, uint256 tokenId)
func (_Token *TokenFilterer) FilterAssetMinted(opts *bind.FilterOpts) (*TokenAssetMintedIterator, error) {

	logs, sub, err := _Token.contract.FilterLogs(opts, "AssetMinted")
	if err != nil {
		return nil, err
	}
	return &TokenAssetMintedIterator{contract: _Token.contract, event: "AssetMinted", logs: logs, sub: sub}, nil
}

// WatchAssetMinted is a free log subscription operation binding the contract event 0x8d1457c1d60a6987eabbac898a50d8d7c81ba604c563663d2807027a4b079054.
//
// Solidity: event AssetMinted(address account, uint256 tokenId)
func (_Token *TokenFilterer) WatchAssetMinted(opts *bind.WatchOpts, sink chan<- *TokenAssetMinted) (event.Subscription, error) {

	logs, sub, err := _Token.contract.WatchLogs(opts, "AssetMinted")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TokenAssetMinted)
				if err := _Token.contract.UnpackLog(event, "AssetMinted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAssetMinted is a log parse operation binding the contract event 0x8d1457c1d60a6987eabbac898a50d8d7c81ba604c563663d2807027a4b079054.
//
// Solidity: event AssetMinted(address account, uint256 tokenId)
func (_Token *TokenFilterer) ParseAssetMinted(log types.Log) (*TokenAssetMinted, error) {
	event := new(TokenAssetMinted)
	if err := _Token.contract.UnpackLog(event, "AssetMinted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TokenOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Token contract.
type TokenOwnershipTransferredIterator struct {
	Event *TokenOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TokenOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TokenOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TokenOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TokenOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TokenOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TokenOwnershipTransferred represents a OwnershipTransferred event raised by the Token contract.
type TokenOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Token *TokenFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*TokenOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Token.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &TokenOwnershipTransferredIterator{contract: _Token.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Token *TokenFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *TokenOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Token.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TokenOwnershipTransferred)
				if err := _Token.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Token *TokenFilterer) ParseOwnershipTransferred(log types.Log) (*TokenOwnershipTransferred, error) {
	event := new(TokenOwnershipTransferred)
	if err := _Token.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TokenTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Token contract.
type TokenTransferIterator struct {
	Event *TokenTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TokenTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TokenTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TokenTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TokenTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TokenTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TokenTransfer represents a Transfer event raised by the Token contract.
type TokenTransfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Token *TokenFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address, tokenId []*big.Int) (*TokenTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Token.contract.FilterLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &TokenTransferIterator{contract: _Token.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Token *TokenFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *TokenTransfer, from []common.Address, to []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _Token.contract.WatchLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TokenTransfer)
				if err := _Token.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_Token *TokenFilterer) ParseTransfer(log types.Log) (*TokenTransfer, error) {
	event := new(TokenTransfer)
	if err := _Token.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
