package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	_ "embed"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"gopkg.in/yaml.v3"
)

//go:embed config.yaml
var configurationData []byte
var configuration Configuration

type Configuration struct {
	Addresses []Address `yaml:"addresses"`
}
type Address struct {
	Name    string `yaml:"name"`
	Address string `yaml:"address"`
}

func init() {
	if err := yaml.Unmarshal(configurationData, &configuration); err != nil {
		log.Fatalf("failed to unmarshal config.yaml: %s", err)
	}
}

func main() {
	log.Println("connecting to rpc...")
	client, err := ethclient.Dial(RpcAddress)
	if err != nil {
		log.Fatalf("failed to connect to the rpc at '%s': %s", RpcAddress, err)
	}
	var existingHash []byte
	if fileInfo, err := os.Lstat(PathToBytecode); err == nil && !fileInfo.IsDir() {
		oldBytecode, err := os.Open(PathToBytecode)
		if err != nil {
			log.Fatalf("failed to read in existing bytecode: %s", err)
		}
		hasher := sha256.New()
		if _, err := io.Copy(hasher, oldBytecode); err != nil {
			log.Fatalf("failed to copy bytecode data into hasher: %s", err)
		}
		existingHash = hasher.Sum(nil)
		log.Printf("existing bytecode sha256: %x\n", existingHash)
	}

	// TODO(z) remember to swap back out
	log.Printf("retrieving contract bytecode for address '%s'...\n", ContractAddressToUse)
	contractAddress := common.HexToAddress(ContractAddressToUse)
	// log.Printf("retrieving contract bytecode for address '%s'...\n", ContractAddressHexMadMeerkatDegen)
	// contractAddress := common.HexToAddress(ContractAddressHexMadMeerkatDegen)
	bytecode, err := client.CodeAt(context.Background(), contractAddress, nil) // nil is latest block
	if err != nil {
		log.Fatalf("failed to get bytecode for contract address'%s': %s", ContractAddressHexMadMeerkatDegen, err)
	}
	if err := ioutil.WriteFile(PathToBytecode, bytecode, os.ModePerm); err != nil {
		log.Fatalf("failed to write new bytecode: %s", err)
	}
	newBytecode, err := os.Open(PathToBytecode)
	if err != nil {
		log.Fatalf("failed to read in current bytecode: %s", err)
	}
	currentHasher := sha256.New()
	if _, err := io.Copy(currentHasher, newBytecode); err != nil {
		log.Fatalf("failed to copy bytecode data into hasher: %s", err)
	}
	currentHash := currentHasher.Sum(nil)
	log.Printf("current bytecode sha256: %x\n", currentHash)

	if existingHash != nil && bytes.Compare(existingHash, currentHash) != 0 {
		log.Printf("⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️")
		log.Printf("BYTE CODE HAS CHANGED")
		log.Printf("⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️")
	} else {
		log.Printf("✅ BYTE CODE IS NEW/SAME AS LAST SEEN ✅")
	}

	log.Printf("retrieving sales data...")
	token, err := NewToken(contractAddress, client)
	if err != nil {
		log.Fatalf("failed to create new token of instance")
	}

	name, err := token.Name(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from name: %s", err)
	}
	log.Printf("chain token name: %v\n", name)

	isActive, err := token.SaleIsActive(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from saleIsActive: %s", err)
	}
	log.Printf("sale is active: %v\n", isActive)

	isPresale1Active, err := token.Presale1Active(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from presale1Active: %s", err)
	}
	log.Printf("presale 1 is active: %v\n", isPresale1Active)

	isPresale2Active, err := token.Presale2Active(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from presale2Active: %s", err)
	}
	log.Printf("presale 2 is active: %v\n", isPresale2Active)

	price, err := token.DegenPrice(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from degenPrice: %s", err)
	}
	humanReadablePrice := new(big.Int).Div(price, big.NewInt(PaymentTokenDecimalPlaces))
	log.Printf("price of nft (public): %s $MAD\n", humanReadablePrice.String())
	presale1price, err := token.DegenPricePresale1(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from degenPrice: %s", err)
	}
	humanReadablePrice = new(big.Int).Div(presale1price, big.NewInt(PaymentTokenDecimalPlaces))
	log.Printf("price of nft (presale 1): %s $MAD\n", humanReadablePrice.String())
	presale2price, err := token.DegenPricePresale1(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from degenPrice: %s", err)
	}
	humanReadablePrice = new(big.Int).Div(presale2price, big.NewInt(PaymentTokenDecimalPlaces))
	log.Printf("price of nft (presale 2): %s $MAD\n", humanReadablePrice.String())

	baseUri, err := token.BaseURI(&bind.CallOpts{})
	if err != nil {
		log.Fatalf("failed to get response from baseUri: %s", err)
	}
	log.Printf("baseUri: %s\n", baseUri)

	log.Printf("checking for whitelist 1...")
	for _, address := range configuration.Addresses {
		var result strings.Builder
		yes, err := token.IsWhitelist1(&bind.CallOpts{}, common.HexToAddress(address.Address))
		if err != nil {
			result.WriteString(fmt.Sprintf("%s: (error: %s)", address.Address, err))
		} else if yes {
			result.WriteString(fmt.Sprintf("%s: \033[32m✅ WHITELISTED\033[0m", address.Address))
		} else {
			result.WriteString(fmt.Sprintf("%s: \033[31m⛔️ NOPE\033[0m", address.Address))
		}
		if address.Name != "" {
			result.WriteString(fmt.Sprintf(" [%s]", address.Name))
		}
		log.Printf(result.String())
	}

	log.Printf("checking for whitelist 2...")
	for _, address := range configuration.Addresses {
		var result strings.Builder
		yes, err := token.IsWhitelist2(&bind.CallOpts{}, common.HexToAddress(address.Address))
		if err != nil {
			result.WriteString(fmt.Sprintf("%s: (error: %s)", address.Address, err))
		} else if yes {
			result.WriteString(fmt.Sprintf("%s: \033[32m✅ WHITELISTED\033[0m", address.Address))
		} else {
			result.WriteString(fmt.Sprintf("%s: \033[31m⛔️ NOPE\033[0m", address.Address))
		}
		if address.Name != "" {
			result.WriteString(fmt.Sprintf(" [%s]", address.Name))
		}
		log.Printf(result.String())
	}

	saleIsActive := make(chan bool, 1)

	var waiter sync.WaitGroup
	// this waits for the sale to become active
	log.Println("starting sales active waiter process...")
	waiter.Add(1)
	go func(every *time.Ticker) {
		count := 0
		fmt.Printf("\nwaiting for sale to be active ....")
		for {
			<-every.C
			isActive, err := token.Presale2Active(&bind.CallOpts{})
			if err != nil {
				fmt.Printf("\n\033[31mfailed to get response from presale2Active: %\033[0m\n", err)
			}
			if (err == nil && isActive) || TestTrigger {
				saleIsActive <- true
				return
			}
			count += 1
			if count%4 == 0 {
				fmt.Println("still waiting...")
			}
		}
	}(time.NewTicker(500 * time.Millisecond))

	log.Println("starting sales active trigger process...")
	// this is triggered when the sale is active
	go func() {
		fmt.Printf("\nwaiting for trigger...\n")
		<-saleIsActive
		fmt.Printf("\nMINT MF!\n")
		fmt.Printf("getting wallet keys...\n")
		keyData, err := ioutil.ReadFile(PathToKeys)
		if err != nil {
			log.Fatalf("keys couldn't be read :/: %s", err)
		}
		var keys map[string]string
		if err := json.Unmarshal(keyData, &keys); err != nil {
			log.Fatalf("keys couldn't be parsed :/: %s", err)
		}
		// var accountKeyMap map[string]ecdsa.PrivateKey
		for _, address := range configuration.Addresses {
			if _, ok := keys[address.Address]; !ok {
				continue
			}
			// grab private key of the configured address (config.yaml address => ./secrets/keys.json key => get value)
			privateKey, err := crypto.HexToECDSA(keys[address.Address])
			if err != nil {
				fmt.Printf("errored out while parsing private keys of address '%s': %s", address.Address, err)
				continue
			}
			// calculate nonce and gas price
			fmt.Println("calculating GAS...")
			fromAddress := crypto.PubkeyToAddress(privateKey.PublicKey)
			nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
			if err != nil {
				fmt.Println("\n\033[31mfailed to get nonce: %s", err)
			}
			gasPrice, err := client.SuggestGasPrice(context.Background())
			if err != nil {
				fmt.Println("\n\033[31mfailed to get suggested gas price: %s", err)
			}
			fmt.Printf("%s: nonce %v | gas %s\n\n", address.Address, nonce, gasPrice.String())

			fmt.Println("binding private key to an auth object...")
			auth := bind.NewKeyedTransactor(privateKey)
			// leave this as 0 since this is a mint (this will be cro otherwise)
			auth.Value = big.NewInt(0) // in wei
			// these we need to modify, adjust the values in constants.go
			auth.GasLimit = uint64(300000) * GasLimitMultiplier // in units
			auth.GasPrice = new(big.Int).Mul(gasPrice, big.NewInt(GasPriceMultiplier))

			if _, err := sendTransaction(token, address, auth, nonce, MaxNumberPerTransaction, presale2price); err != nil {
				fmt.Printf("errored for address[%s] calling presaleMintDegen2: %s", address.Address, err)
			}
		}
		fmt.Println("ending waiter...")
		waiter.Done()
	}()

	// this estimates gas
	log.Println("starting gas/nonce information process...")
	go func(every *time.Ticker) {
		for {
			<-every.C
			var results string
			for _, address := range configuration.Addresses {
				var result strings.Builder
				result.WriteString(address.Address + ": ")
				nonce, err := client.PendingNonceAt(context.Background(), common.HexToAddress(address.Address))
				if err != nil {
					fmt.Println("\n\033[31mfailed to get nonce: %s", err)
				}
				gasPrice, err := client.SuggestGasPrice(context.Background())
				if err != nil {
					fmt.Println("\n\033[31mfailed to get suggested gas price: %s", err)
				}
				result.WriteString(fmt.Sprintf("nonce: %v | gas: %s\n", nonce, gasPrice.String()))
				results += result.String()
			}
			fmt.Printf("\nBEGIN nonce/gas update\n%sEND nonce/gas update\n", results)
		}
	}(time.NewTicker(10 * time.Second))
	waiter.Wait()

	log.Println("hope you got it 🍻")
}

func sendTransaction(token *Token, address Address, auth *bind.TransactOpts, nonce uint64, numberToMint int64, price *big.Int) (*types.Transaction, error) {
	fmt.Println("performing transactions...")
	fmt.Println("--- executing transaction ---")
	auth.Nonce = big.NewInt(int64(nonce))
	fmt.Println("mint transaction is sending...")
	numberOfNftsToMint := big.NewInt(numberToMint)
	numberOfTokensToUse := new(big.Int).Mul(price, numberOfNftsToMint)
	tx, err := token.PresalemintDegen2(auth, numberOfNftsToMint, numberOfTokensToUse)
	if err != nil {
		return nil, err
	}
	transactionData := tx.Data()
	transactionHash := tx.Hash()
	fmt.Printf("transaction[%s] sent\n", transactionHash.String())
	transactionDataAsHex := common.Bytes2Hex(transactionData)
	fmt.Printf("transaction[%s] data input: %s\n", transactionHash.String(), transactionDataAsHex)
	fmt.Printf("transaction[%s] can be viewed at: https://cronoscan.com/tx/%s\n", transactionHash.String(), transactionHash.String())

	return tx, nil
}
