# DegenMINT

> Just a fun project to learn more about how all these smart contracts work. do not use this for evil or harm

# Context

This project was created to explore automations around smart contracts for my own learning. And also because I really wanted a Degen Mad Meerkat. The state of this is currently super hacky and tailored for my own whitelist (presale 2 for CRO kats) benefit. Change things around as needed!

# Risks

**This project requires that your private keys be exported onto a digital medium**. If possible, use a hot throwaway wallet with this to mint, send over the nfts to your main account and never touch the minting account again. you have been warned!

# Usage

## Dependencies

1. You need Golang installed, get the `go` binary from https://go.dev/doc/install
2. Run `go mod tidy` and `go mod vendor` to pull in the dependencies, these should be at `./vendor` if you're not familiar with Go

## Configuration

There are two configuration files (paths are relative to the project root):

- `./config.yaml`
- `./secrets/keys.json`

### `config.yaml`

Make a copy of the `config.yaml.sample` as `config.yaml` and populate it. A sample document is included with fake values. This file consists of a root property `addresses` which is a list of address objects. Each address object has the properties `name` and `address` which are both strings. The `name` is an arbitrary label you can use to label your wallet, and `address` is the hex address of your wallet.

An example follows:

```yaml
addresses:
  - name: mmf
    address: "0x97749c9b61f878a880dfe312d2594ae07aed7656"
  - name: mmm
    address: "0x49ab7ca4D2Cf6777c8C56C5E9Edb7D1218AE1299"
  - name: mmo
    address: "0x50c0c5bda591bc7e89a342a3ed672fb59b3c46a7"
```

### `./secrets/keys.json`

Make a copy of the `./secrets/keys.json.sample` as `./secrets/keys.json` and populate it. it should be a map of (`account address => private key`). given these are your private keys, please run `chmod 600` on them or secure them in some other way. my recommendation is to avoid putting in your actual keys until the mint is happening. run the script with the keys populated and then overwrite it a few times with fake values before deleting it (remember to empty your trash too)

An example follows:

```json
{
  "0xabcdefabcdefabcdefabcdefab0123456789abcd": "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef1234",
  "0xabcdefabcdefabcdefabcdefab0123456789abcd": "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef1234"
}
```

**The addresses in this JSON file MUST also be found in the `./config.yaml`**

## Actually using this

### Pre-run configurations

Since this is a hack overall, the configurations are found in [`./constants.go`](./constants.go) as constant values. Here are some things you might wanna change:

1. Topup your CRO, by default we use 2x the chain's recommmended gas fees to frontrun a gas price war given it's Mad Meerkats
2. The gas limit multiplier can be adjusted via the `GasLimitMultiplier` value. It is set at `2` but if you're running this a second time for the second transaction, set this to `3` or `4` even to get your transaction processed quicker
3. The gas price multiplier does the same thing - I'm not proficient enough to appreciate the differences - and can be configured through the `GasPriceMultiplier` constant value. Similar to above, it's set to `2` but you can change this to `3` or `4` in times of need
4. Ensure that `TestTrigger` constant value is set to `true` for the mint day itself or you could risk wasting transactions.
5. If you don't trust the VVS Finance RPC (the only thing they got right IMO), you can change that via the `RpcAddress` constant value too before running.

If you're using this but you're on the ETH kats whitelist, you need to navigate into [`./main.go`](./main.go), navigate to around line 196 and there should be a line looking like:

```go
// ...
isActive, err := token.Presale2Active(&bind.CallOpts{})
// ...
```

Change `token.Presale2Active` to `token.Presale1Active`. Also find all other instances of `token.PresalemintDegen2` and change them all to `token.PresalemintDegen1`

I don't believe there'll be a public sale so just buy your degen kat from me if you're in the public mint xD

# Other notes for self

1. ABI wasn't published this time unlike for [MadMeerkatTreehouse](https://cronoscan.com/token/0xdc5bbdb4a4b051bdb85b959eb3cbd1c8c0d0c105#writeContract) which has its ABI published meaning you can call it from the contract there. It took some digging but the ABI at [`./src/abi.json`](./src/abi.json) was found in the JavaScript source code of [the mint page](https://madmeerkat.io/mint)
2. Having the ABI means that Go source code can be generated to call the chain directly, which is what you see at [`./abi.go`](./abi.go). To regenerate that you can run `make generate_go`
3. With the ABI, the rest is purely Web2 bot programming, code is pretty linearly structured and should be obvious to the reader
4. The main difficulty encountered was not knowing what the second variable of the `PresalemintDegenX` was supposed to be. This was solved by going to the website, forcing the button to be enabled, and doing a stack trace within the file `mint-63cac0383ada9c465f27.js` till I got to the place where the same method was being called. By setting the number to be minted to 2, the second variable turned up as `110000000000000000000` so all is good (it was `55000000000000000000` when set to 1)
5. With the ABI and some sample transactions, you can use [Ethereum input data decoder
](https://lab.miguelmota.com/ethereum-input-data-decoder/example/) to verify the transaction input data
6. The values from the above will be in hexadecimal, to see what the value is in decimal, this [Hexadecimal to Decimal Converter](https://www.binaryhexconverter.com/hex-to-decimal-converter) was used
7. Sample successful transaction assuming the sale was live can be found here https://cronoscan.com/tx/0x3904912103f75f06e78fbdcf9a044d9171ea53ff22631c5ca780c927249b1bac
