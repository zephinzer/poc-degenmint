package main

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common"
)

const sampleTransactionData = "0xcfb3bdfd0000000000000000000000000000000000000000000000000000000000000002000000000000000000000000000000000000000000000002fb474098f67c0000"
const sampleTreehouseData = "0xf23f65700000000000000000000000000000000000000000000000000000000000000005"

func main() {
	fmt.Printf("degen data:     %v\n", common.FromHex(sampleTransactionData))
	fmt.Printf("treehouse data: %v\n", common.FromHex(sampleTreehouseData))
}
